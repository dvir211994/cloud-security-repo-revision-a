### Lab: Creating an AMI using Packer

##### Objectives:
- Install Packer on an EC2 instance
- Create a packer.json configuration file
- Build an AMI using Packer

##### Steps:

1. Create an EC2 (t2.micro) instance and SSH into that instance

2. Install Packer on the EC2 Instance

Solution:
IyBBZGQgdGhlIEhhc2hpQ29ycCBHUEcga2V5IGFuZCBhZGQgdGhlIG9mZmljaWFsIEhhc2hpQ29ycCBMaW51eCByZXBvc2l0b3J5CmN1cmwgLWZzU0wgaHR0cHM6Ly9hcHQucmVsZWFzZXMuaGFzaGljb3JwLmNvbS9ncGcgfCBzdWRvIGFwdC1rZXkgYWRkIC0Kc3VkbyBhcHQtYWRkLXJlcG9zaXRvcnkgImRlYiBbYXJjaD1hbWQ2NF0gaHR0cHM6Ly9hcHQucmVsZWFzZXMuaGFzaGljb3JwLmNvbSAkKGxzYl9yZWxlYXNlIC1jcykgbWFpbiIKCiMgVXBkYXRlLCBhbmQgaW5zdGFsbCBQYWNrZXIKc3VkbyBhcHQtZ2V0IHVwZGF0ZSAmJiBzdWRvIGFwdC1nZXQgaW5zdGFsbCBwYWNrZXIKCiMgVmVyaWZ5IHRoZSBpbnN0YWxsYXRpb24gd2FzIHNlY2Nlc2Z1bGwKcGFja2VyIC0tdmVyc2lvbgoKIyBFeHBvcnQgdGhlIGFjY2VzcyBrZXkgYW5kIHNlY3JldCBrZXkgSURzIG9mIHlvdXIgQVdTIGFjY291bnQKZXhwb3J0IEFXU19BQ0NFU1NfS0VZX0lEPSIgIgpleHBvcnQgQVdTX1NFQ1JFVF9BQ0NFU1NfS0VZPSIgIgo=

4. Create a packer.json File (name your file "packer.json")
* You can find an example `packer.json` file in the lab folder, tweak it to change configurations inside - change the AMI from Centos to Ubuntu and update the `provisioners` section accordingly

5. Verify the configurations you entered are correct - using `packer validate`

Hint:
cGFja2VyIHZfX19fX190ZSBwYWNrZXIuanNvbg==

6. Build an AMI Using packer.json

Solution:
cGFja2VyIGJ1aWxkIC12YXIgJ2FtaV9uYW1lPWFtaS08VVNFUk5BTUU+JyAtdmFyICdiYXNlX2FtaT08QU1JX0lEPicgLXZhciAndnBjX2lkPTxWUENfSUQ+JyAtdmFyICdzdWJuZXRfaWQ9PFNVQk5FVF9JRD4nIHBhY2tlci5qc29uCg==

* Notice that we can pass parameters using the command line, which has the highest precedence

7. Once the command has completed (it could take up to 5–10 minutes), copy the AMI ID from the output and verify that it exists in your AWS account under the EC2 AMI section (or via the AWS CLI)

##### Submission:
- Submit the AMI ID that was created from the process

Bonus challenge:

Create a shell script which will

- Get positional parameters for the ami_name, base_ami, vpc_id, subnet_id
- Validate the script and if the packer.json file and if it is ok, the script will run the build
- Save the output of the build AMI ID to a variable and verify using AWS CLI if the AMI ID is ready for use (can use the UNTIL cmdlet)
- When the AMI is ready for use, run an AWS CLI / Terraform to create a t2.micro instance out of that AMI