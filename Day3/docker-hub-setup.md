### Setting up your workstation

1. Browse to [https://hub.docker.com/signup](https://hub.docker.com/signup)

2. Open the Docker hub and create an account

3. In your terminal type `docker login` and enter your username and password

4. To verify that it works for you run `docker pull alpine` and then run `docker images` , you should be able to see the Docker image you just downloaded called Alpine

* Note that now your username and password are stored with base64 encryption, in your home folder under .docker/config.json , make sure this file is only accessible by the docker user and your personal user