### Demo - Creating a Flask web server using Dockerfile

##### Objectives:
- Create a Dockerfile which will package our Flask application
- Create an image from our Dockerfile
- Run a container from the built image and verify it works

##### Steps:

1. Create a new folder for the project `mkdir -p /tmp/flask-web-app && cd /tmp/flask-web-app`

2. Creating a simple `app.py`
    - Flask application, which will listen on port 5000
    - The app should have 2 routes,
        1. "/" - when getting to the root level, the user is greeted with "Hi there!"
        2. "/book/<ISBN>" - under the book route, provide a book`s ISBN code, the API should respond with information regarding the book via JSON
    - Create the fetching of the book`s metadata using try catch and raise the relevant exception in case of failure
Bonus:
    - Create a machenisem for logging the servers logs to a local file
    - Implement a 3rd route, /cover_image, get an ISBN from the user and view an HTML file including the JSON and cover image of the book
    
3. Creating our `requirements.txt` file
    - Find the relevant libraries you want to load to the project and include the in the 
    - For your local tests, make sure that you install the requirements before you start testing, `pip3 install -r requirements.txt`

 4. Create a `Dockerfile` which will do the following
    - Create the image out of a base image you see fit for the need of this web-server
    - Pack the source code files that you created
    - Run specific commands inside the container to make sure the code there runs
