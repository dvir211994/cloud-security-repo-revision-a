import json
import requests
from flask import Flask

app = Flask(__name__)
url = 'http://openlibrary.org/api/books?bibkeys=ISBN:ISBN_PlaceHolder&jscmd=details&format=json'

@app.route("/")
def hello():
    return "Hello World!"

@app.route('/book/<ISBN>')
def get_book_info(ISBN):
    try:
        Updated_URL = url.replace('ISBN_PlaceHolder', ISBN)
        output = requests.get(Updated_URL).json()
        json_object = json.dumps(output, indent = 4)   
        return json_object
    except Exception as ex:
        return "### Can't find the book you searched for.." + str(ex) + " ###"

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
