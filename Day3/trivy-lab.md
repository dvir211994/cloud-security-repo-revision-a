### Lab - checking our docker image for vulnerabilities

##### Objectives:
- Running SAST tool on our docker image
- Analysing the results of the report and fixing vulnerabilities

After we have created our docker image using Dockerfile or pulling an image from the docker hub,
before using this container in our production environment we want to verify that this container has as less security issues as possible

---

Steps:

1. Run the `docker images` command to list all of the docker images that exist on you workstation

2. Download the Trivy vulnerability scanning tool via
`git clone [https://github.com/knqyf263/trivy](https://github.com/knqyf263/trivy)`
can also be done via `brew install aquasecurity/trivy/trivy`

3. Run the Trivy tool against the docker image you want to test, you can use an existing image on your workstation or pulling a new one from Docker Hub
* example image https://hub.docker.com/r/bkimminich/juice-shop

4. Examine the report and verify which CVE`s your docker image is subjected to

5. Google search one of the critical vulnerabilities and try to understand how it can be exploited

Bonus challenge:
- Try to fix one of the vulnerabilities you see in the report
* most simple method would be to just upgrade one of the vulnerable packages
- Run the tool again, and verify that the previous error you handled does not exist anymore