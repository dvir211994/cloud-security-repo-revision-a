### Lab - Create a private Docker registry

##### Objectives:
- Create a private Docker Registry with self-signed certificate
- Run push and pull from a client workstation

##### Steps:

1. Create 2 EC2 instances (t2.micro) in the same VPC
    1. Docker registry instance
    2. Docker client
    * Install Docker on both instances 

2. SSH to your Docker registry server

3. Create an `htpasswd` file to store the login credentials for accessing the registry

Solution:
bWtkaXIgLXAgL0RvY2tlci1yZWdpc3RyeS9hdXRoZW50aWNhdGlvbgpkb2NrZXIgcnVuIC0tZW50cnlwb2ludCBodHBhc3N3ZCByZWdpc3RyeToyLjcuMCAtQmJuICVVc2VyTmFtZSUgJVBhc3N3b3JkJSA+IC9Eb2NrZXItcmVnaXN0cnkvYXV0aGVudGljYXRpb24vaHRwYXNzd2Q=

4. Create a self-signed certificate for the Registry, in our example since the traffic is only internal we can use the internal `hostname` of the machine, no need for a FQDN or domain name
to know the hostname of your Docker Registry instance run the following command: `hostname`

Solution:
bWtkaXIgLXAgL0RvY2tlci1yZWdpc3RyeS9jZXJ0aWZpY2F0ZXMKb3BlbnNzbCByZXEgXAogICAtbmV3a2V5IHJzYTo0MDk2IC1ub2RlcyAtc2hhMjU2IC1rZXlvdXQgL0RvY2tlci1yZWdpc3RyeS9jZXJ0aWZpY2F0ZXMvZG9tYWluLmtleSBcCiAgIC14NTA5IC1kYXlzIDM2NSAtb3V0IC9Eb2NrZXItcmVnaXN0cnkvY2VydGlmaWNhdGVzL2RvbWFpbi5jcnQK

5. Run the registry container on your Docker Registry instance

Solution:
ZG9ja2VyIHJ1biAtZCAtcCA0NDM6NDQzIC0tcmVzdGFydD1hbHdheXMgLS1uYW1lIHJlZ2lzdHJ5IFwKICAgLXYgL0RvY2tlci1yZWdpc3RyeS9jZXJ0aWZpY2F0ZXM6L2NlcnRzIFwKICAgLXYgL0RvY2tlci1yZWdpc3RyeS9hdXRoZW50aWNhdGlvbjovYXV0aCBcCiAgIC1lIFJFR0lTVFJZX0hUVFBfQUREUj0wLjAuMC4wOjQ0MyBcCiAgIC1lIFJFR0lTVFJZX0hUVFBfVExTX0NFUlRJRklDQVRFPS9jZXJ0cy9kb21haW4uY3J0IFwKICAgLWUgUkVHSVNUUllfSFRUUF9UTFNfS0VZPS9jZXJ0cy9kb21haW4ua2V5IFwKICAgLWUgUkVHSVNUUllfQVVUSD1odHBhc3N3ZCBcCiAgIC1lICJSRUdJU1RSWV9BVVRIX0hUUEFTU1dEX1JFQUxNPVJlZ2lzdHJ5IFJlYWxtIiBcCiAgIC1lIFJFR0lTVFJZX0FVVEhfSFRQQVNTV0RfUEFUSD0vYXV0aC9odHBhc3N3ZCBcCiAgIHJlZ2lzdHJ5OjIuNy4wCg==

6. On the Docker client, we need to add the Docker Registry public self-signed certificate, in order to do that we can use 2 methods

    A. SCP - copy the public certificates between the two instances using SSH

Solution:
c3VkbyBzY3AgdWJ1bnR1QCVEb2NrZXItUmVnaXN0cnktSG9zdG5hbWUlOi9Eb2NrZXItcmVnaXN0cnkvY2VydGlmaWNhdGVzL2RvbWFpbi5jcnQgL2V0Yy9kb2NrZXIvY2VydHMuZC8lRG9ja2VyLVJlZ2lzdHJ5LUhvc3RuYW1lJTo0NDM=

    B. VIM - copy the content of the file from the terminal (`cat` the contents of the public certificate on the Registry, copy it, create a file using VIM on the Docker client instance and paste the content there)

8. On the Docker client, perform a login to the Docker Registry instance
`docker login %Docker-Registry-Hostname%`

* Enter the username and password created in step #4

9. Test pulling and pushing images to and from the Docker Registry
While on the client instance, run the following commands

Solution:
IyBQdWxsIGEgc3BlY2lmaWMgRG9ja2VyIGltYWdlIGZyb20gdGhlIERvY2tlciBSZWdpc3RyeQpkb2NrZXIgcHVsbCB1YnVudHUKCiMgVGFnIHRoZSBpbWFnZSB5b3UganVzdCBkb3dubG9hZGVkLCBiYXNpY2FsbHkgbWFraW5nIGl0IHVuaXF1ZQpkb2NrZXIgdGFnIHVidW50dSAlRG9ja2VyLVJlZ2lzdHJ5LUhvc3RuYW1lJTo0NDMvY2hhbmdlZC1pbWFnZToxCgojIFB1c2ggdGhlIG5ldyB1bmlxdWUgaW1hZ2UgdG8gdGhlIERvY2tlciBSZWdpc3RyeQpkb2NrZXIgcHVzaCAlRG9ja2VyLVJlZ2lzdHJ5LUhvc3RuYW1lJTo0NDMvY2hhbmdlZC1pbWFnZTox

10. Verify image pulling by deleting the image locally and re-pulling it from the private repository

Solution:
IyBEZWxldGluZyB0aGUgbmV3IGltYWdlIHdlIGNyZWF0ZWQsIHRvIGxhdGVyIHB1bGwgaXQgYWdhaW4sIGFzIGFuIGluZGljYXRpb24gdGhhdCB0aGUgUmVnaXN0cnkgc3RvcmVkIGl0CmRvY2tlciBpbWFnZSBybSAlRG9ja2VyLVJlZ2lzdHJ5LUhvc3RuYW1lJTo0NDMvY2hhbmdlZC1pbWFnZToxCgojIFB1bGwgdGhlIGltYWdlIHdlIGp1c3QgZGVsZXRlZCBsb2NhbGx5IGJ1dCBpcyBzdGlsbCBzdG9yZWQgYXQgdGhlIFJlZ2lzdHJ5CmRvY2tlciBwdWxsICVEb2NrZXItUmVnaXN0cnktSG9zdG5hbWUlOjQ0My9jaGFuZ2VkLWltYWdlOjE=