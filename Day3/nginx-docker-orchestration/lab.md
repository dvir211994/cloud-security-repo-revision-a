### Lab - Create a container orchestration using Docker Swarm mode

##### Objectives:
- Create a Swarm cluster
- Configure the Manager and Worker nodes
- Join the Worker node to the cluster
- Create a new service and verify it exists on the Worker node

##### Steps:

1. Create 2 EC2 instances (t2.micro) in the same VPC
* if you wish, you can do it also using the AWS CLI
    1. First instance will be the Swarm master
    2. Second instance will be the Swarm worker

2. SSH into both instances and install Docker on them

3. On the master start by initialising the Swarm

Solution:
ZG9ja2VyIHN3YXJtIGluaXQgLS1hZHZlcnRpc2UtYWRkciAlTUFOQUdFUl9QUklWQVRFX0lQJQ==

4. After we run this command on the master, we should get as an output a `docker swarm join` command which will allow workers to join the Swarm, copy that command

5. On the worker instance, run the `join` command to add it to the cluster

Hint:
ZG9ja2VyIHN3YXJtIGpvaW4gLS10X19fX19fX24gW1RPS0VOXSBfX19fX19fX19fXzoyMzc3

6. On the master instance, verify that Swarm mode is enabled and that you can see the worker instance successfully joined the cluster
`docker node ls`

7. Now, let's create a new Swarm service (Nginx)

Hint:
ZG9ja2VyIHNlcnZpY2UgY3JlYXRlIC1kIFwNCi0tbmFtZSBfX19fX19fX19fX19fIFwNCi1wIDgwODA6ODAgXA0KLS1yZXBsaWNhcyBfX19fX19fX19fX18gXA0Kbl9fX19feDpsYXRlc3Q=

Here we are creating a new service which will be based on the `nginx:latest` docker image and requested the Master to have the desired state of two containers for this service at any given time, scattered throughout our cluster

8. Now, let's verify the service we just created is healthy and the Master has created containers for it on the different nodes (Master and Worker)

`docker service ls` - this command will show us all the running services we have configured in our cluster

`docker service ps nginx_web_app` - this command will show us all the deployed containers under this service and on which node each one of them is running

9. Expose port 8080 on the Master instance in AWS VPC and Security-Group configurations

10. Connect to both containers, using `docker exec -it %ContainerID% bash` from the node running the container, and change the `index.html` file to have an indication that the browser has hit a specific server (e.g add a <title> HTML tag and call it Server1 in a specific container, and on the other container call it Server2)

11. Use your browser to access the public IP of the Master node (you can find this data from the EC2 console), make sure that you can see the nginx greeting page, hit refresh and see that each time you are working against a different backend-server (through the indication created from step 10)

##### Submission:
- Submit the public IP of your Swarm master which is exsposing the 8080 port


##### Bonus challenge:

- Push the docker image we created in `Dockerfile-demo` to the Docker Hub
- When creating the Swarm service use the nginx image
- Create an additional EC2 instance or an RDS instance which will be available only from within your AWS VPC, that means it can not be reached from the internet
- Create a new service on the Swarm called "Squid_Service", use this squid as a proxy to route traffic coming to the Master to be routed through the squid to the private instance (EC2/RDS) that is not available from the internet
- Create an nginx security rule for restricting access to the squid proxy only to specific addresses by their geographical location (e.g Singapore) - this should not affect our nginx web service, we want everyone to access it
- Use a cluster visualise to visualize your Swarm (https://hub.docker.com/r/dockersamples/visualizer)