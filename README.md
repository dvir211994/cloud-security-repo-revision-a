# Cloud-Security-Course-Draft

Hi there, welcome to the "Cloud Security Course" repository

In here you can find:
- Labs
- Demos
- Walkthroughs

Notes:
* The exercises are indexed per day
* If you find any typo or error, please feel free to open a PR
* Want to contribute anything or share? that's great, send a PR
* Feel free to fork this repository


[<img src="https://www.memesmonkey.com/images/memesmonkey/c9/c907650a4e9dba128e52d9ade2f7603c.jpeg">](https://www.memesmonkey.com/images/memesmonkey/c9/c907650a4e9dba128e52d9ade2f7603c.jpeg)
