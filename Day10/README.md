# CI/CD Pipeline - Python Web Application - DevOps Course Lab


## Lab overview:

#### 1) Develop a Python web-app with REST API
A) Develop the web-app on your local workstation and push to your GitLab repo  
B) Version tagging and artifact pushing to on-premises Docker Hub on AWS  
C) The app will be scalable and will store and receive data from RDS  

#### 2) Continuous integration with Jenkins and GitLab
A) Push to master branch will trigger a container build  
B) Jenkins job will run pre-configured unit tests automatically upon trigger  
	
#### 3) Create the ELK stack on AWS to gather metrics on deployed containers
A) Install and configure heartbeat and metricbeat as part of the Dockerfile  
B) Create a dashboard in Kibana to show KPIs and business logic metrics  
C) Create an APM dashboard and analyze data from the application run-time metrics

#### 4) Configure EC2 instances with Auto Scaling Groups
###### Create and configure the following AWS resources
*	VPC  
*	Elastic Load Balancer  
*	Target Groups
*	Security Group
*	AWS Key Pair
*	AWS Access and Secret API keys
*	EC2 Autoscaling Groups
*	RDS

#### 5) Continuous Deployment to AWS using Terraform and Ansible
###### Create a Dockerfile with the following:  
*	Elastic beats  
*	Python web-app solution  
*	APM Python client  
*	Create Terraform configuration file
*	Create an Ansible playbook and deploy the software on the instances
*	Create a Jenkins job to run Terraform and Ansible combination

#### 6) RDS instance creation using AWS Console/Terraform & KMS
A) Create an RDS production instance and configure a read-only replica
B) Integrate your python web-app to work with the RDS instance


---
---
---

## Lab Objectives

Upon completion of this Lab you will be able to:

* Develop a web-application with REST API using Python and Flask
* Working with AWS RDS instances as data store for your application
* Implement unit tests and integration tests, including Linter
* Create ELK stack instance on AWS with APM server
  * Installing and configuring agents (Elastic beats + APM agent) on endpoints
* Configure integration between GitLab and Jenkins for CI purposes
* Create and configure Jenkins pipeline and freestlye jobs, triggered by GitLab webhook
*	Create and configure environments and resources on AWS using Terraform and AWS console
  * RDS, VPC, NLB, TG, ASG, EC2, KMS, SG
* Run Ansible playbooks, using inventory to dynamically run commands on remote AWS instances
*	Configure network and security infrastructure practices needed to deploy a highly available website

## Lab Prerequisites

You should feel comfortable with the following:

*	Working with the command-line in Linux
* Familiarity with JSON and YAML formats
* Basic networking understanding
* Bash scripting and text manipulation tools
* Python development
* SSH
* Git basic operations

---
---
---


## Lab Architecture:


<img src="readme-resources/Lab-Architecture2-1.png"  width="1000" height="1000">

---
---
---



## Workstation prerequisites:
#### Install Docker, Git, Python3, AWS CLI, Terraform, Ansible, and configure as follows:

###### GitLab:
```shell
ssh-keygen -t rsa # Create RSA key pair in case you don't have one
# Make sure to upload your public key to gitlab https://docs.gitlab.com/ee/ssh/README.html#adding-an-ssh-key-to-your-gitlab-account
# Add the following to your ~/.ssh/config file
Host gitlab.com
 Preferredauthentications publickey
 IdentityFile $PathToYourSSHKey
```

###### AWS CLI:
```shell
Get your AWS access key and secret key ID
# Once you have access keys you can run this also via CLI
# Get a list of the IAM users in your AWS account and find your username
aws iam list-users | grep UserName
# Create an access key for your user
aws iam create-access-key --user-name Bob
 
# Configure your local aws credentials (can be found under ~/.aws/credentials)
aws configure # You will be prompted to insert the following
AWS Access Key ID [None]: $YourAWSAccessKeyID
AWS Secret Access Key [None]: $YourAWSSecretKeyID
Default region name [None]: ap-southeast-1
Default output format [None]: json
```

###### Docker:
```shell
# Create a user at https://hub.docker.com/
# Login to Docker hub to be able to download Docker images to work on
docker login --username=yourhubusername --email=youremail@company.com
```

###### Terraform:
```shell
sudo apt install software-properties-common
sudo apt-add-repository --yes --update ppa:ansible/ansible
sudo apt install ansible
```

###### Ansible:
```shell
# Install Terraform and Ansible
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update && sudo apt-get install terraform
```

---
---
---



## Develop a Python web-app with REST API - Students information creation and retrieval using Python, Flask, and MySQL

#### Create a workplace on your local workstation

###### 1) Run a Ubuntu container using
```shell
docker run --privileged -p 5000:5000 -it ubuntu bash # We need to run in privileged mode since we are going to run iptables
```
This command will run a container in interactive terminal mode which will be exposing port 5000, and will pull the latest ubuntu image from the Docker registry (if it does not exist already in local images).  
To escape the container without killing PID 1 use “ctrl + p + q

###### 2) Exec into the container to install additional requirements
```shell
docker ps
docker exec -it $ContainerID bash
# Can be shortened to “docker exec -it $(docker ps -q) bash” in case of only one container running on the workstation
```

###### 3) Inside the container install Python2.7, Flask, and Flask RESTful
```shell
apt-get update
apt install python2.7 python-pip -y
pip install flask flask-restful # Can use “pip install -r requirements.txt”
```

###### 4) Allow remote connections
```shell
apt install net-tools -y
apt-get install vim curl iptables -y
iptables -I INPUT -p tcp --dport 5000 -j ACCEPT # Allowing remote connections
```

---
---
---



## Develop the Python web-app using Flask and flask-restful


###### The solution should perform the following:  
1)	Accept HTTP requests over port 5000  
2)	Designed to support CRUD (GET, POST, UPDATE, DELETE)  
3)	Insert/update/delete data to a PostgreSQL DB  
	-	Attributes of a student should be: Name, Age, Hobby  
	-	Find a suitable Python library to work with the DB, you can consider SQLAlchemy and PyMySQL
4)	Provide API responses for the following requests:  
	-	List all current students (return in JSON form)  
	-	Insert a new student into the database  
	-	Update attributes for a specific student (by StudentID)  
	-	Return error message if the student does not exist  
		-	Delete an entry of a student  
		-	Return error if the student does not exist  
5)	Expose HealthCheck API to have service status indication  
6)	Include a logger for the app that writes to a local log file  
7)	Include APM agent to send metrics to APM-server in ELK  

###### Once development of the web-app is ready, verify functionality:


Run the Python web-app
```shell
python api.py
```
Run a localhost curl to verify you get a list of all students
```shell
curl -L localhost:5000/students
```
You should expect JSON output of the existing students in class  
Run a localhost curl to verify you can list a specific student
```shell
curl -X PUT -L http://127.0.0.1:5000/students/3
```
Run a localhost curl to verify you can insert a new student
```shell
curl -X PUT -L http://127.0.0.1:5000/students/?name=David?age=22?spec=math
```
Run a localhost curl to verify you can update a student`s info
```shell
curl -X PUT -L http://127.0.0.1:5000/students/3?name=Jacob?age=22?spec=physics
```


---
---
---



## Setting up your AWS environment (via AWS Console/Terraform)


##### Create a VPC using AWS Console:

Open the Amazon VPC console at https://console.aws.amazon.com/vpc  
From the top right menu navigate to ‘ap-southeast-1’ region (Singapore)  

<img src="readme-resources/vpc1.png"  width="400" height="400">

Navigae to “Your Virtual Private Cloud’
Click on “Create VPC”

<img src="readme-resources/vpc2.png"  width="500" height="300">

Create the VPC with the following configurations:

*	Name tag = my-vpc-sin
*	IPv4 CIDR block = 192.168.0.0/24
*	IPv6 CIDR block = No IPv6 CIDR block
*	Tenancy = Default


##### Create a VPC using Terraform:

VPC Creation
```terraform
resource "aws_vpc" "main" {
 cidr_block           = "${var.vpc_cidr}"
 enable_dns_hostnames = true
 enable_dns_support   = true
 tags {
   Name = "staging-vpc"
   Environment = "staging"
 }
}
```


##### Create a Target Group for the scaled EC2 instances

Navigate to EC2 service
Under “Load Balancing” choose Target Groups
Choose Create target group
Create the Target Group with the following configurations:
*	Target Type = Instances
*	Target group name = tg-sin
*	Protocol = HTTP
*	Port = 5000
*	Health check path = /HealthCheck
Do not register any targets yet, we will add those automatically


##### Create a Target Group using Terraform:

```terraform
resource "aws_lb_target_group" "python-web-app-staging-tg" {
 health_check {
   interval            = 10
   path                = "/HealthCheck"
   protocol            = "HTTP"
   timeout             = 5
   healthy_threshold   = 5
   unhealthy_threshold = 2
 }
 name        = "staging-tg"
 port        = 80
 protocol    = "HTTP"
 target_type = "instance"
 vpc_id      = "${var.vpc_id}"
}
```


##### Create a Network Load Balancer using AWS Console:

Navigate to EC2 service
Under “Load Balancing” choose Load Balancers
Choose Create Load Balancer
Network Load Balancer
Create the NLB with the following configurations:
*	Name = python-web-app
*	Scheme = internet-facing
*	Listeners = TCP
*	Port = 5000
*	Availability Zones = ap-southeast-1a, ap-southeast-1b, ap-southeast-1c
*	Target group = Select existing “tg-sin”
*	Protocol = TCP


##### Create a Network Load Balancer using Terraform:

```terraform
resource "aws_lb" "staging-python-web-app-lb" {
 name               = "staging-lb"
 internal           = false
 load_balancer_type = "network"
 subnets            = [aws_subnet.public.*.id]
 enable_deletion_protection = true
 tags = {
   Environment = "staging"
 }
}
```

##### Create a Security group using AWS Console

Navigate to EC2 service
Under “Network & Security” choose Security Groups
Select “Create Security Group”
*	Create the SG with the following configurations:
	*	Security group name = “sg-sin-python-web-app”
	*	Description = “SG for course lab”
	*	VPC = select the VPC you created previously
*	Inbound rules (protocol, port, source, description)
	*	TCP, 5000, Anywhere, “python web app global access”
	*	TCP, 8080, My IP, “Jenkins server access”
	*	TCP, 22, My IP, “SSH to EC2 instances - Ansible”
	*	TCP, 5601, My IP, “Access to Kibana UI”
	*	TCP, All ports, VPC Internal traffic, “For APM, Elastic, SSH etc.”


##### Create a Security group using Terraform

```terraform
# Please add additional ingress rules as SSH example
resource "aws_security_group" "test_sg" {
 name   = "my-test-sg"
 vpc_id = "${aws_vpc.main.id}"
}
# Ingress Security Port 22
resource "aws_security_group_rule" "ssh_inbound_access" {
 from_port         = 22
 protocol          = "tcp"
 security_group_id = "${aws_security_group.test_sg.id}"
 to_port           = 22
 type              = "ingress"
 cidr_blocks       = ["%YourPublicIP%/0"]
}
# All OutBound Access
resource "aws_security_group_rule" "all_outbound_access" {
 from_port         = 0
 protocol          = "-1"
 security_group_id = "${aws_security_group.test_sg.id}"
 to_port           = 0
 type              = "egress"
 cidr_blocks       = ["0.0.0.0/0"]
}
```


##### Create a key pair - using AWS CLI

```shell
aws ec2 create-key-pair --key-name MyKeyPair --query 'KeyMaterial' --output text > MyKeyPair.pem
```

##### EC2 Autoscaling groups

Prerequisites:

1)	Create an Amazon AMI from the EC2 instance you deployed your containerized version of your solution
2)	Decide on Instance type
3)	Run benchmark to understand how much time it takes your solution to boot on the specified instance type from Prerequisites#2

###### Steps:

Creating the Launch Template

*	Go to EC2 console -> Launch template
*	Select AMI from prerequisite #1 and select the required instance type
*	Select you AWS key pair
*	Select VPC and storage

Creating the Auto Scaling Group

*	Go to EC2 console -> Launch template
*	Select the Launch Template you created in step “Creating the Launch Template”
*	Select the required subnets the ASG is going to run in (recommended > 1)
*	Configure “Health check grace period” as per how much seconds it usually takes your containerized solution to boot on the instance type given in the ASG
*	Select “Enable load balancing” -> Network Load Balancer -> specify the Target Group we created using Terraform
*	Enable “Monitoring” to send metrics to CloudWatch
*	Group size:
	*	Desired = 3
	*	Minimum = 2
	*	Maximum = 5
*	Add notifications (optional) - configure SNS topic to send mail/Slack notifications


---
---
---


## Creating a Jenkins build server on Ubuntu

Login to the Amazon EC2 console at https://console.aws.amazon.com/ec2/
Go to the EC2 service -> Launch instance



<img src="readme-resources/jenkins1.png"  width="800" height="300">  




On “Choose an Amazon Machine Image” search for “Ubuntu Server 18.04 LTS”, select it, and launch the instance with the following configurations:  
* Instance type = t2.micro  
* Auto-assign Public IP = Enable  
* VPC = VPC you created in step “Setting up your AWS environment”  
* Storage = 8GB (General Purpose SSD (gp2))  
* Tags =  
  * “Name” = “Jenkins-CI-Server”  
* Security Group = SG from step “Setting up your AWS environment”  
* Key pair = key pair from step “Setting up your AWS environment”  
* Launch the instance  
* Wait for the instance to be in “running” state under “Instance State”  


<img src="readme-resources/jenkins2.png"  width="850" height="200">


---
---
---



## Connecting to your Jenkins server for installation and configuration

Open your terminal and type the following to SSH into your Jenkins instance

ssh ubuntu@$JenkinsPublicIP -i $aws-pem-key-path


Now you should be connected via ssh to your Jenkins server

Run the following commands in order to install and configure Jenkins
```shell
# Since Jenkins is a Java based we will need to install Java, along with Python for our app
apt install default-jre python2.7 python-pip -y

# Now we will install Jenkins
wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
sudo sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > \
   /etc/apt/sources.list.d/jenkins.list'
sudo apt-get update
sudo apt-get install jenkins

# Starting the Jenkins service and verify its status
sudo systemctl start jenkins

# Install APM
curl -L -O https://artifacts.elastic.co/downloads/apm-server/apm-server-7.9.1-amd64.deb
dpkg -i apm-server-7.9.1-amd64.deb
apt update
apm-server setup --index-management
sudo -u apm-server apm-server

# Install Terraform and Ansible
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update && sudo apt-get install terraform
sudo apt install software-properties-common
sudo apt-add-repository --yes --update ppa:ansible/ansible
sudo apt install ansible

# Install Docker
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
sudo usermod -aG docker jenkins

# Install AWS CLI
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
apt install unzip
unzip awscliv2.zip
sudo ./aws/install

# Switch to jenkins user and generate SSH keys, add those to GitLab SSH Keys
sudo su jenkins
ssh-keygen -t rsa

# Install boto for ec2 plug-in in Ansible
pip install boto3
pip install botocore
```

---
---
---


## Accessing Jenkins post-installation and running the configuration wizard

Open browser and access Jenkins UI via the following URL:
http://%JenkinsPublicIP%:8080/
Go back to the SSH terminal and type the following to get the initial admin password:
```shell
cat /var/lib/jenkins/secrets/initialAdminPassword
```
Paste the output of the previous command in the Jenkins wizard page
Choose Install suggested plugins
Create a first admin user following the wizard
Instance configuration: make sure that the configured external IP is correct
Save and finish -> Start using Jenkins

---
---
---

## Creating an RDS instance using AWS Console/Terraform and KMS


#### Using the AWS console:
Connect to AWS console and search for the “RDS” service
Click on “Create Database”
Create an RDS instance with the following configurations:
        database creation method = Standard Create
        Engine options = PostgreSQL
        Templates = Free tier
DB instance identifier = pgsql-python-web-app
Master username = postgres
Storage = SSD, 20GB
Storage autoscaling = Disabled
        VPC = Choose your VPC
        Database authentication = Password authentication
        Initial database name = students
        Backup = Disabled
        Performance Insights = Disabled
        Maintenance =
                Enable auto minor version upgrade = Disabled


#### Using Terraform and KMS:

Create KMS keys
```terraform
resource "aws_kms_key" "rds-key" {
   description = "key to encrypt rds password"
 tags {
   Name = "my-rds-kms-key"
 }
}

resource "aws_kms_alias" "rds-kms-alias" {
 target_key_id = "${aws_kms_key.rds-key.id}"
 name = "alias/rds-kms-key"
}
```


Use the created key to encrypt a secret inside the command line
```shell
aws kms encrypt --key-id <kms key id> --plaintext admin123 --output text --query CiphertextBlob
```

Pass the encoded string from last command output as a payload
```terraform
data "aws_kms_secret" "rds-secret" {
 "secret" {
   name = "master_password"
   payload = "payload value here"
 }
}

resource "aws_db_instance" "my_test_mysql" {
 allocated_storage           = 20
 storage_type                = "gp2"
 engine                      = "mysql"
 engine_version              = "5.7"
 instance_class              = "${var.db_instance}"
 name                        = "pgsql-python-web-app"
 username                    = "admin"
 password                    = "${data.aws_kms_secret.rds-secret.master_password}"
 parameter_group_name        = "default.mysql5.7"
 db_subnet_group_name        = "${aws_db_subnet_group.rds-private-subnet.name}"
 vpc_security_group_ids      = ["${aws_security_group.rds-sg.id}"]
  multi_az                    = false
}
```

---
---
---


## Continuous integration with Jenkins and GitLab:


Configure GitLab to trigger a Webhook for each Merge Request to ‘master’ branch:

1)	Install the GitLab plug-in on Jenkins
	*	Go to ‘Manage Jenkins’ -> ‘Manage Plugins’ -> click on ‘Available’ tab
	*	Select and install ‘GitLab Plugin’

<img src="readme-resources/jenkins3.png"  width="750" height="500">

2)	Add SSH key pair credentials to Jenkins
Go to ‘Manage Jenkins’ -> ‘Credentials’ -> ‘System’ -> ‘Add Credentials’
Provide the private key you have generated in step “Connecting to your Jenkins server for installation and configuration”

<img src="readme-resources/jenkins4.png"  width="750" height="500">  
  
* Make sure to insert passphrase if created the key pair with a password  


3)	Create a GitLab access token
Go to https://gitlab.com/profile/personal_access_tokens
Create a personal access token and provide expiration date
* Save this token, it won’t be accessible again via GitLab
Configure the token in jenkins
Go to ‘Manage Jenkins’ -> ‘Credentials’ -> ‘System’ -> ‘Add Credentials’

<img src="readme-resources/jenkins5.png"  width="600" height="400">


4)	Create your CI pipeline job
Go to Jenkins dashboard and select “New Item”

<img src="readme-resources/jenkins6.png"  width="600" height="400">

Add the groovy code to the pipeline - example code can be taken from /Infrastructure/CI-Pipeline-Jenkinsfile  
Go to ‘Configure’ in the job settings pane, and configure the build triggered by Webhood


<img src="readme-resources/jenkins7.png"  width="600" height="400">


5)	Create a Webhook configuration on GitLab
From the GitLab project you want to build, hover on “Settings” on the left pane and select the “Webhooks” option

<img src="readme-resources/jenkins8.png"  width="800" height="700">



---
---
---


## Configure ELK stack on AWS EC2 instance:


Create the EC2 instance
Note that you need at least 2 vCPU and 4 GB RAM
SSH to EC2 instance to configure and install the following:
```shell
# Install Java
sudo apt-get update
sudo apt-get install openjdk-8-jdk
# Download and install public signing key
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
# Install apt-transport-https package
sudo apt-get install apt-transport-https -y
# Save directory definitions
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list
# Update and Install elasticsearch
sudo apt-get update && sudo apt-get install elasticsearch && sudo apt-get install logstash && sudo apt-get install kibana
```

Modify Elasticsearch configuration file
```shelll
# File located at /etc/elasticsearch/elasticsearch.yml
# change cluster name
cluster.name: aws-elk-cluster
# give the cluster a descriptive name
node.name: aws-node
# change network binding
network.host: 0.0.0.0 # To allow remote connections, we listen on the external IP
# setup discovery.type as single node
discovery.type: single-node
```

Start Elasticsearch service
```shell
sudo systemctl start elasticsearch
```

validate Elasticsearch cluster health
```shell
curl -XGET http://localhost:9200/_cluster/health?pretty
```
* You should get a JSON output with cluster information


Modify Kibana configuration file
```shell
# File is located at /etc/kibana/kibana.yml
# Remove comment from server.port and edit as follows
server.port: 5601
# Change server.host
server.host: "0.0.0.0"
# Change server.name
server.name: "aws-kibana-server"
# Remove comment from elasticsearch.host and edit as follows
elasticsearch.hosts: ["http://localhost:9200"]
```

Start Kibana service
```shell
systemctl start kibana
```

Enable Elasticsearch and Kibana (to enable the services at boot)
```shell
systemctl enable elasticsearch
systemctl enable kibana
```

---
---
---


### After ELK is configured and your solution has been deployed with the beat agents and APM, you should expect to see data in those dashboards

###### APM Dashboard:  


<img src="readme-resources/apm.png"  width="1000" height="600">


###### APM Discover:  


<img src="readme-resources/apm-discover.png"  width="1000" height="600">


###### Heartbeat Discover:  


<img src="readme-resources/heartbeat.png"  width="1000" height="600">


###### Metricbeat Discover:  


<img src="readme-resources/metricbeat.png"  width="1000" height="600">


---
---
---



## Continuous Deployment to AWS using Terraform and Ansible


Create a Dockerfile to pack the built code from GitLab

```shell
# Use FROM to configure the container base image to build on

# Use RUN to navigate the file system and create required folders

# Use COPY to copy the content of your GitLab repo to working folder on the container

# Use RUN to execute warmup

# Use ENV to create any environment variables like APM and RDS configurations

# Use CMD to specify the entry point for the container
```


Hint:
```dockerfile
FROM ubuntu # Choose from which docker image to create the container
RUN mkdir -p /etc/org/python-web-app
WORKDIR /etc/org/python-web-app
COPY . /etc/org/python-web-app # Copy content of code repo to the container
ENV FLASK_APP=/etc/org/python-web-app/api.py # Create relevant ENV variables
CMD ["sh", "-c", "./warmup.sh"] # Set you ENTRYPOINT
```
