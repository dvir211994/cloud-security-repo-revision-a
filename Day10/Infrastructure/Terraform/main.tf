provider "aws" {
    profile = "dvir-personal"
    region = "${var.aws_region}"
    shared_credentials_file = "${var.cred_file}"
}

# Create the AWS EC2 instance
# Task: make the resource create more than 1 instance, hints can be found inside the resource

resource "aws_instance" "python-web-app-backend-instances" {
    #count   =   2
    ami =   "ami-0b44582c8c5b24a49"
    instance_type = "t2.micro"
    key_name =  "${var.key_name}"
    tags = {
    Name = "web-app-backend" #-${count.index + 1}
    Environment = "Staging"
    Purpose = "staging-backends"
    }
}

# Create a Target Group

resource "aws_lb_target_group" "python-web-app-staging-tg" {
 health_check {
   path                = "/HealthCheck"
   protocol            = "HTTP"
}
 
 name        = "staging-tg"
 port        = 5000
 protocol    = "TCP"
 target_type = "instance"
 vpc_id      = "${var.vpc_id}"
}

# Register the created EC2 instance to the created Target Group
resource "aws_alb_target_group_attachment" "python-web-app-staging-tg-instances" {
  target_group_arn = "${aws_lb_target_group.python-web-app-staging-tg.arn}"
  target_id        = "${aws_instance.python-web-app-backend-instances.id}"
  port             = 8080
  # count            = 2
}

# Create an NLB Load-Balancer

resource "aws_lb" "python-web-app-staging-lb" {
  name               = "Staging-LB"
  internal           = false
  load_balancer_type = "network"
  enable_deletion_protection = false
  subnets = [
  "${var.subnet1}",
  "${var.subnet2}",
  "${var.subnet3}",
  ]
  tags = {
    Environment = "Staging"
  }
}

# Add a listner to the created NLB Load-Balancer

resource "aws_lb_listener" "python-web-app-lb-listner" {
  load_balancer_arn = "${aws_lb.python-web-app-staging-lb.arn}"
  port              = "5000"
  protocol          = "TCP"
  default_action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.python-web-app-staging-tg.arn}"
  }
  depends_on = ["aws_lb_target_group.python-web-app-staging-tg"]
}
