#!/bin/bash

# Checkout to desired branches
git clone git@gitlab.com:dvir211994/python-web-app-infra.git
cd ./python-web-app-infra
git checkout $Infrastructure_Version

git clone git@gitlab.com:dvir211994/python-web-app.git
cd ../python-web-app
git checkout $Solution_Tag

# Validate configuration file
python ./Tests/linter.py

# Run Unit tests
python ./Tests/unit-test.py

# Build docker image from the solution version we cloned and save it to be shipped with Ansible
docker build -t python-web-app:$Solution_Tag .
docker image save --output /tmp/python-web-app:$gitlabMergeRequestId python-web-app:$gitlabMergeRequestId

# Deploy to production environment using Ansible (Terraform not needed in CD in this lab)
ansible-playbook -i Production-inventory.aws_ec2.yml playbook.yml

# Verify E2E testing to make sure production is aligned and functioning as expected
# Write test plans and execute end to flow testing flow, from external and internal traffic, think of edge cases

# Workspace cleanup
rm -rf /var/lib/jenkins/workspace/CD-Production/