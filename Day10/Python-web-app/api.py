import json
import logging
from flask import Flask
from logger import logger
from flask_restful import Resource, Api, reqparse
from elasticapm.contrib.flask import ElasticAPM

# Trying to load configurations from configuration file
try:
    with open("./config.json", 'r') as json_file:
        data = json.load(json_file)
except Exception as e:
    print(e)

# TODO: add a machenisem for retrieving sensitive data from S3 or Ansible Vault instead of clear text
# Load configurations
RDSuser = (data['Configurations']['RDS']['user'])
RDSpass = (data['Configurations']['RDS']['pass'])
RDSurl = (data['Configurations']['RDS']['url'])
APMuser = (data['Configurations']['APM']['user'])
APMpass = (data['Configurations']['APM']['pass'])
APMurl = (data['Configurations']['APM']['url'])

logger.info("Configuration loaded seccesfully")

# Initialize the flask application
app = Flask(__name__)
app.config['ELASTIC_APM'] = {
          'SERVICE_NAME': 'python-web-app',     
          'SERVER_URL': 'http://3.237.14.145:8200'
}
apm = ElasticAPM(app, logging=True)

# Generate error by force to show it in APM
@app.route('/zerodivision')          
def zerodivision():             
    try:
        1 / 0
    except ZeroDivisionError:
        apm.capture_exception()

api = Api(app)

# Mock data - please implement a way to manage this information in the RDS

STUDENTS = {
    '1': {'name': 'Jacob', 'age': 27, 'spec': 'math'},
    '2': {'name': 'Nathan', 'age': 34, 'spec': 'math'},
    '3': {'name': 'Ben', 'age': 42, 'spec': 'math'},
    '4': {'name': 'Monica', 'age': 35, 'spec': 'math'},
}

parser = reqparse.RequestParser()

# Creating classes

class StudentsList(Resource):
    def get(self):
        return STUDENTS

class HealthCheck(Resource):
    def get(self):
        return 'STATUS UP', 200

    def post(self):
        parser.add_argument("name")
        parser.add_argument("age")
        parser.add_argument("spec")
        args = parser.parse_args()
        student_id = int(max(STUDENTS.keys())) + 1
        student_id = '%i' % student_id
        STUDENTS[student_id] = {
            "name": args["name"],
            "age": args["age"],
            "spec": args["spec"],
        }
        return STUDENTS[student_id], 201

class Student(Resource):
    def get(self, student_id):
            if student_id not in STUDENTS:
                return 'Not found', 404
            else:
                return STUDENTS[student_id]

    def put(self, student_id):
        parser.add_argument("name")
        parser.add_argument("age")
        parser.add_argument("spec")
        args = parser.parse_args()
        if student_id not in STUDENTS:
            return 'Record not found', 404
        else:
            student = STUDENTS[student_id]
            student["name"] = args["name"] if args["name"] is not None else student["name"]
            student["age"] = args["age"] if args["name"] is not None else student["age"]
            student["spec"] = args["spec"] if args["name"] is not None else student["spec"]
            return student, 200

    def delete(self, student_id):
        if student_id not in STUDENTS:
            return 'Not found', 404
        else:
            del STUDENTS[student_id]
            return '', 204


# Adding and configuring resources paths

api.add_resource(StudentsList, '/students/')
api.add_resource(Student, '/students/<student_id>')
api.add_resource(HealthCheck, '/HealthCheck/')

logger.info("Application initialized")

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')