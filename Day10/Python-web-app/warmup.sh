apt-get update
apt install python2.7 python-pip -y # Python needed for the web-app and flask
apt-get install vim curl iptables -y # Install curl for local tests and iptables
pip install -r requirements.txt
apt-get install --reinstall systemd -y
hostname=$(hostname)
echo "Host name is: $hostname"

#####################
# Heartbeat - Installation and configuration
#####################
# Install Heartbeat
curl -L -O https://artifacts.elastic.co/downloads/beats/heartbeat/heartbeat-7.9.1-amd64.deb
dpkg -i heartbeat-7.9.1-amd64.deb

# sed Elasticsearch - Please change the value instead of being hard coded to dynamic from enviromant variables
sed -i 's/localhost:9200/3\.237\.14\.145:9200/g' /etc/heartbeat/heartbeat.yml

# sed Kibana - Please change the value instead of being hard coded to dynamic from enviromant variables
sed -i 's/localhost:5601/3\.237\.14\.145:5601/g' /etc/heartbeat/heartbeat.yml
KibanaHostLine=$(cat -n /etc/heartbeat/heartbeat.yml | grep '"3.237.14.145:5601"' | awk -F' ' '{print $1}')'s'
sed -i "$KibanaHostLine/#/ /" /etc/heartbeat/heartbeat.yml

# Implement configuration of Heartbeat monitors to verify HealthCheck - https://www.elastic.co/guide/en/beats/heartbeat/current/configuration-heartbeat-options.html

# Set up assets
heartbeat setup -e

# Start Heartbeat + watch dog to verify service is up
service heartbeat-elastic start
systemctl enable heartbeat-elastic

#####################
# Metricbeat - Installation and configuration
#####################
# Install Metricbeat
curl -L -O https://artifacts.elastic.co/downloads/beats/metricbeat/metricbeat-7.9.1-amd64.deb
dpkg -i metricbeat-7.9.1-amd64.deb

# sed Elasticsearch - Please change the value instead of being hard coded to dynamic from enviromant variables
sed -i 's/localhost:9200/3\.237\.14\.145:9200/g' /etc/metricbeat/metricbeat.yml

# sed Kibana - Please change the value instead of being hard coded to dynamic from enviromant variables
sed -i 's/localhost:5601/3\.237\.14\.145:5601/g' /etc/metricbeat/metricbeat.yml
KibanaHostLine=$(cat -n /etc/metricbeat/metricbeat.yml | grep '"3.237.14.145:5601"' | awk -F' ' '{print $1}')'s'
sed -i "$KibanaHostLine/#/ /" /etc/metricbeat/metricbeat.yml

# Enable required Metricbeat modules
metricbeat modules enable system linux
metricbeat setup -e

# Start Metricbeat
service metricbeat start
systemctl enable metricbeat

python /etc/org/python-web-app/api.py