import boto3
import json
import time

QUEUE_URL = 'https://sqs.us-east-1.amazonaws.com/150946214737/test-q'

# Create SQS client
boto3.setup_default_session(profile_name='dvir-personal')
sqs = boto3.client('sqs', region_name='us-east-1')

with open('data.json', 'r') as f:
    data = json.loads(f.read())

for i in data:
    msg_body = json.dumps(i)
    response = sqs.send_message(
        QueueUrl=QUEUE_URL,
        MessageBody=msg_body,
        DelaySeconds=2,
        MessageAttributes={
            'Office': {
                'DataType': 'String',
                'StringValue': 'Iceland'
            },
            'Producer': {
                'DataType': 'String',
                'StringValue': 'Producer-Instance-1'
            }
        }
    )
    print(response)