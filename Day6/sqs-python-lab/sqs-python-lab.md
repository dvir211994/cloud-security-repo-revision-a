### Lab - Sending and receiving messages in SQS Standard queues

##### Objectives:
- Working with the AWS SDK to send and receive messages in SQS Standard queues

##### Steps:

1. Create an SQS queue using AWS CLI, Terraform or AWS Console

2. Use the AWS CLI or AWS Python SDK to do the following:

- Send dummy content messages
- Receive all the messages sent and "process" them (print)
- Delete the messages you successfully processed

Bonus challenge:

- Change the default visibility timeout of the queue to 60 seconds using the AWS CLI
- Create 10 containers with the same shared volume, each will send the same data, each container should spin up, send 50 messages from a JSON file and exit
- Spin up 10 containers to process together all of the messages from the queue
- Create a dead-letter-queue and simulate a scenario where messages are not able to be processed which makes the messages end up in the dead-letter-queue