import boto3
import json
import time

QUEUE_URL = 'https://sqs.us-east-1.amazonaws.com/150946214737/test-q'

# Create SQS client
boto3.setup_default_session(profile_name='dvir-personal')
sqs = boto3.client('sqs', region_name='us-east-1')

i = 0

while i < 10000:
    i = i + 1
    rec_res = sqs.receive_message(
        QueueUrl=QUEUE_URL,
        MessageAttributeNames=[
            'All',
        ],
        MaxNumberOfMessages=1,
        VisibilityTimeout=5,
        WaitTimeSeconds=10
    )
    del_res = sqs.delete_message(
        QueueUrl=QUEUE_URL,
        ReceiptHandle=rec_res['Messages'][0]['ReceiptHandle']
    )
    print("RECIEVED MESSAGE :")
    print('FROM PRODUCER: ' + rec_res['Messages'][0]['MessageAttributes']['Producer']['StringValue'])
    print('FROM OFFICE LOCATION: ' + rec_res['Messages'][0]['MessageAttributes']['Office']['StringValue'])
    print('BODY: ' + rec_res['Messages'][0]['Body'])
    print("DELETED MESSAGE")
    print("")
    time.sleep(1)