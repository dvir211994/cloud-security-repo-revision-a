variable "rds_subnet1" {
  default = "subnet-0e3e0544"
}
variable "rds_subnet2" {
  default = "subnet-5f017503"
}
variable "db_instance" {
  type    = string
  default = "db.t2.micro"
}
variable "vpc_id" {
  default = "vpc-980a8ee2"
}
