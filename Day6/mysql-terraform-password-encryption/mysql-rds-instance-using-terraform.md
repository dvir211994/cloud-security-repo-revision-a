### Demo - Creating a MySQL RDS instance using Terraform

##### Objectives:
- Create a MySQL RDS instance using Terraform
- Running Terraform validations on your configuration files
- Working with AWS KMS to work with encrypted variables

##### Steps:

1. Log in to the AWS Console and fetch the following information:
    * VPC ID - the VPC where you want to deploy your RDS instance in
    * Subnet ID - 2 subnet IDS as In order to create a new MySql database we first need to create a subnet group and assign at least two subnets to it

2. Create a new Terraform project folder and run `terraform init`

3. Create 2 new files `main.tf` and `variables.tf` (included in this folder)

4. Run `terraform init` again and then run `terraform plan`, go over the plan and see check that you approve it

5. Run `terraform apply` to start the deploy, go over the plan again and submit 'yes' into the console

6. Verify that no erros are shown in the terminal and the deploy is going as expected

7. Once the deployment is finished, log in to AWS Console and verify that you can see the newly created instance


Bonus: 

- Create an `outputs.tf` file which will, output the value of the AWS RDS instance identifier and endpoint name at the end of the deploy
XXXXXXXX



---

### Now, we have a bit of an issue here from security standpoint, can you see what is it?
We are passing the password in plain text form, we need to find a way to send it encrypted over the network
For that need we can use AWS KMS, let's see how we can intergrate it into our project files



##### Steps:

1. Creating our KMS keys, here we will create a KMS key called "my-rds-kms-key" and assign an alias to it called "alias/rds-kms-key"
    * Note that aliases have to start with the prefix of `alias/`

``` terraform
provider "aws" {
  region = "us-east-1"
  profile = "dvir-personal"
}

resource "aws_kms_key" "rds-key" {
    description = "key to encrypt rds password"
}

resource "aws_kms_alias" "rds-kms-alias" {
  target_key_id = "${aws_kms_key.rds-key.id}"
  name = "alias/dvir-kms-key"
}
```

2. Now, let's use this key to encrypt a secret password, we will do it using AWS CLI

``` shell
aws kms encrypt --key-id ${Your-Alias-Here} --plaintext ${Secret-Password-Here} --output text --query CiphertextBlob
# Example
# aws kms encrypt --key-id alias/dvir-kms-key --plaintext Password1 --output text --query CiphertextBlob --profile dvir-personal --region us-east-1
```
* Now, we should get an encoded string to pass as a payload to the terraform project file



---


3. Create a new project folder with 2 files `main.tf` and `variables.tf`, and then run `terraform init`
main.tf file content:

``` terraform
provider "aws" {
  region = "us-east-1"
  profile = "dvir-personal"
}

data "aws_kms_secrets" "rds-secret" {
  secret {
    name = "master_password"
    payload = "Put the payload you got from step 2 command output here"
  }
}

resource "aws_db_instance" "my-test-sql" {
  instance_class          = "${var.db_instance}"
  engine                  = "mysql"
  engine_version          = "5.7"
  multi_az                = true
  storage_type            = "gp2"
  allocated_storage       = 20
  name                    = "mytestrds"
  username                = "admin"
  password                = data.aws_kms_secrets.rds-secret.plaintext["master_password"]
  apply_immediately       = "true"
  backup_retention_period = 10
  backup_window           = "09:46-10:16"
  db_subnet_group_name    = "${aws_db_subnet_group.my-rds-db-subnet-new.name}"
  vpc_security_group_ids  = ["${aws_security_group.my-rds-sg-new.id}"]
  skip_final_snapshot     = "true"
}

resource "aws_db_subnet_group" "my-rds-db-subnet-new" {
  name       = "my-rds-db-subnet-new"
  subnet_ids = ["${var.rds_subnet1}", "${var.rds_subnet2}"]
}

resource "aws_security_group" "my-rds-sg-new" {
  name   = "my-rds-sg-new"
  vpc_id = "${var.vpc_id}"
}

resource "aws_security_group_rule" "my-rds-sg-rule" {
  from_port         = 3306
  protocol          = "tcp"
  security_group_id = "${aws_security_group.my-rds-sg-new.id}"
  to_port           = 3306
  type              = "ingress"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "outbound_rule" {
  from_port         = 0
  protocol          = "-1"
  security_group_id = "${aws_security_group.my-rds-sg-new.id}"
  to_port           = 0
  type              = "egress"
  cidr_blocks       = ["0.0.0.0/0"]
}
```


---


variables.tf file content:
``` terraform
variable "rds_subnet1" {
  default = "subnet-0e3e0544"
}
variable "rds_subnet2" {
  default = "subnet-5f017503"
}
variable "db_instance" {
  type    = string
  default = "db.t2.micro"
}
variable "vpc_id" {
  default = "vpc-980a8ee2"
}
```

4. After the init was completed, run `terraform validate` to validate the Terraform files

5. If the validation is correct, go ahead and deploy the instance, using `terraform apply`







