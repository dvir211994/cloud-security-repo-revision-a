### Lab - Create an EC2 with MongoDB on it and query the DB

##### Objectives:
- Install a MongoDB server
- Import a data source to the DB
- Run simple queries on the DB

##### Steps:

1. Create an EC2 instance (using AWS Console, AWS CLI, Terraform or Ansible)

2. SSH into the instance and install MongoDB
https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/

3. Start the MongoDB service
``` shell
sudo service mongod start
```

4. Copy the data source from the repo into your instance (vim, scp, wget github raw etc.)

5. Import the data source into MongoDB

```shell
mongoimport -d WorldCities -c cityinfo --type CSV --file cities.csv --headerline
```

6. Use the `mongo` shell in order to interact with the DB

7. Run queries on the database

```shell
# Example
mongo # To get into the mongo command line client
use cities # This is the name of the DB we created from the import command
db.cityinfo.find({State:"GA"}) # cityinfo is the name of the collection we created in the import command
```

- Fetch all the cities that have the "iso3" column equal to "SGP"
- Fetch all the cities that have the "iso3" equal to "JPN" and "population" is bigger than 3797700
- Display all the capital cities (https://simplemaps.com/data/world-cities)


Bonus challenge:

- Create a cluster of MongoDB servers using docker-compose





