### Demo - Local Kafka environment - send and receive messages

##### Objectives:

- Understand the basics of Kafka as a queue
- Produce and consume messages to a specific topic using Kafka command-line tools
- View the messages using Kafdrop UI or the Kafka command-line tools


##### Steps:

1. Create a `docker-compose.yaml` file
``` yaml
version: "2"
services:
  kafdrop:
    image: obsidiandynamics/kafdrop
    restart: "no"
    ports:
      - "9000:9000"
    environment:
      KAFKA_BROKERCONNECT: "kafka:29092"
    depends_on:
      - "kafka"
  kafka:
    image: obsidiandynamics/kafka
    restart: "no"
    ports:
      - "2181:2181"
      - "9092:9092"
    environment:
      KAFKA_LISTENERS: "INTERNAL://:29092,EXTERNAL://:9092"
      KAFKA_ADVERTISED_LISTENERS: "INTERNAL://kafka:29092,EXTERNAL://localhost:9092"
      KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: "INTERNAL:PLAINTEXT,EXTERNAL:PLAINTEXT"
      KAFKA_INTER_BROKER_LISTENER_NAME: "INTERNAL"

```


2. Spin up both of the containers mentioned in the Docker compose file `docker-compose up`

3. Browse to localhost:9000, you should see the Kafdrop - Kafdrop is a web UI for viewing Kafka topics and browsing consumer groups. The tool displays information such as brokers, topics, partitions, consumers, and lets you view messages

4. Exec into the Kafka container using `docker exec -it kafka_1 bash`

5. While in the container run the following

``` shell
# Change directory to the folder containing Kafka command-line tools
cd /opt/kafka/bin

# Produce test messages
./kafka-console-producer.sh --broker-list localhost:9092 --topic test
# Once this command is running you will be prompted to insert a message, hit "ENTER" to send a message, and press CTRL+D when done

```

6. You can also view the `test` topic in the Kafdrop UI, by browsing to http://localhost:9000/ (Click on the topic name -> 'View Messages)

7. To consume the messages from the command line you can use

``` shell
# This command will tail the topic from the first record, press CTRL-C to end
./kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic test --group test --from-beginning
```



Bonus challenge:

- Use any Kafka client to produce and consume messages https://cwiki.apache.org/confluence/display/KAFKA/Clients
- Send 1k messages in batch, and then consume those messages, calculate the round trip duration, compare the result to AWS SQS