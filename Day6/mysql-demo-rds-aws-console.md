### Demo - Creating a MySQL RDS insatnce on using the AWS Console

##### Objectives:
- Create a MySQL RDS instance on AWS using the AWS console

##### Steps:

1. Log in to your AWS Console

2. Go to the RDS service

3. Select the following RDS configurations
    - Engine: Mysql
    - Templates: Free tier
    - DB instance class: db.t2.micro
    - Storage type: SSD (20gb)
    - DB instance identifier: MySQLRDSinstance
    - Master username: admin
    - Master password: MyStrongPassword
    - VPC: select your current VPC
    - Multi AZ: false
    - Public accessibility: false
    - Monitoring: Disable enhanced monitoring

4. Select the RDS instance we just created and, create a read replica for it

5. SSH to an existing EC2 instance in the same VPC and try to access the RDS instances (you can do so using `mysql-client` or Python MySQL client)
    - Access the main RDS instance and verify that you are able to perform read and write operations
    - Access the read-replica instance and verify that you can only perform read operations
        * For example, you can run the command `CREATE DATABASE database_name;` and make sure that you get an error for the server being running on --read-only mode
