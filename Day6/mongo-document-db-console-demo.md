### Demo - creating a Document db instance on AWS Console

##### Objectives:
- Create a MongoDB database on AWS using the AWS Console

##### Steps:

1. Log in to your AWS Console

2. Go to the "DocumentDB" service, and provision a cluster
Create the cluster with the following configurations:
    - Instance class: db.t3.medium
    - Number of instance: 1
    - Advanced settings -> Deletion protection -> Enable deletion protection: false
      
3. Wait for the cluster to be created by AWS

4. Create an EC2 instance or SSH into an existing one

5. Install the mongo-shell client on that instance

``` shell
# Import the public key used to sign packages from MongoDB, Inc
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4

# Create a package list file for MongoDB
echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.0.list

# Update your local package database
sudo apt-get update

# Install the desired MongoDB package(s)
sudo apt-get install -y mongodb-org-shell

```

6. Go back to the cluster on AWS Console, now that the cluster is ready for use you should see a section in the cluster configuration called "coonect", follow the instructions on that section in order to connect to the DB

7. Run sample queries to see that you are able to work with the DB instance
Example:

``` shell
# Insert an example document
db.col.insert({hello: "Hola world"})

# Run a query to retrieve the document
db.col.find()
```