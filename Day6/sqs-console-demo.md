### Demo - Creating a sample SQS queue using AWS console

##### Objectives:
- Creating an SQS standard queue on AWS using the AWS console


##### Steps:

1. Log in to your AWS console

2. Go to the SQS service

3. Create a "Standard Queue" with default configurations and no dead-letter-queue

4. Go over to the queue created, and examine the details gives, copy the queue URL for use with the next lab (Sending and receiving messages in SQS Standard queues)