### Lab - AWS Lambda

##### Objectives:
- Creating a Lambda function
- Testing the function is working
- Reviewing CloudWatch logs

##### Steps:

1. Log in to the AWS console and select the Singapore region

2. Under services search for Lambda and click it

3. Click Create a function

To create a function we have three options
    A. we can author it from scratch - and it starts with a simple hello world example
    B. we can use a blueprint - and this will use some kind of predefined lambda function for us
    C. we can browse a serverless app repository - which contains a bunch of apps created by the public
* We are going to use a blueprint

4. In the "Blueprint" search field, search for "Hello", this will give you two options, choose the "Python3.7" options, and click on configure
Give a name for the function, "Hola-world"
Now, let's choose an execution role, a role is an IAM role that will be attached to our Lambda function that will allow us to execute actions for our Lambda function for example integration with services, like EC2, S3 etc.

5. IAM Role:
Create a new Role with 'Basic Lambda permissions' -> the console will show us the name of the Role that will be created and state that it will have permissions to upload logs to Amazon CloudWatch -> which basically just allows Lambda to log what it does to CloudWatch

6. Function code:
In the function code, we can already see a simple function Lambda code that is predefined since we are using a Blueprint
In the code we have a function (which is a lambda_handler) which is going to print contents of a JSON that our function will recieve as input
Don't change anything yet in the code and click on "Create function"

7. Testing
Click on test and create a new test event
Set the event template to "hello-world"
Name the test event "EventSample" - you can see the event is predefined with sample JSON data
Now, if we click on "Test" we should be able to test our Lambda function
Verify that the output is as expected

8. Logs
Examine the execution result log from the Lambda console
Under "Log output" click on the "Click here" to view the logs on CloudWatch and examine the log stream

9. Code change
Go back to the Lambda console and edit the code of your function
Comment the line which returns the key from the JSON
Uncomment the line that raises an exception
Save the code change you made and run the test again
Make sure that the execution result is failed and that you can see the error message from your code

##### Submission:
Please submit the final output you got from running the Lambda function
* Once reviewed and approved, you can delete the Lambda function from your account

##### Bonus challenge:

- Create a Lambda function which mimics a connection to an API server
- Set environment variables such as 'hostURL', 'user', 'Token' - make sure to create the ENV variables encrypted using KMS
- Implement Python code in the function which will paste the plain text of those variables
"Initiating a secure connection to the API server, using %hostURL%, as the hostURL, %user% as the user name, and %Token% as the secure API token"
- Implement a dummy try catch, and run tests against the code twice using a test template
    1. The code should be executed successfuly and the variables are printed
    2. An error is thrown (deliberately), stating the "connection to the API server has failed"
