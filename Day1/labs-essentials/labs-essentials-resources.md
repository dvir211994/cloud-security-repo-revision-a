Bash commands cheat sheet
https://devhints.io/bash
https://goalkicker.com/BashBook/BashNotesForProfessionals.pdf

Passwordless SSH access
https://linuxize.com/post/how-to-setup-passwordless-ssh-login/

Working with VIM
https://opensource.com/article/19/3/getting-started-vim

Text manipulation tools
https://www.youtube.com/watch?v=BMrL7zoyJH8
https://www.youtube.com/watch?v=91msRzo0VYw