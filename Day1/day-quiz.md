### Day 1 quiz

1. What is the process that automatically compiles files into a useable form called?
    a. Build automation
    b. Monitoring
    c. Continuous deployment
    d. Orchestration

2. What are small, loosely-coupled services that make up a larger application called?

    a. Serverless services
    b. Microservices
    c. Orchestrated services
    d. Cloud services

3. Which practice helps you quickly identify problems in production?

    a. Monitoring
    b. Orchestration
    c. Microservices
    d. Continuous integration

4. What is DevSecOps?

    a. Integrating security practices within the DevOps process
    b. Securing the DevOps team
    c. Making sure that the our source has not syntax errors
    d. Automation that supports processes and workflows

5. What does infrastructure as code mean?
    a. Autoscaling
    b. Going serverless
    c. Manually setting up servers
    d. Managing infrastructure using code and automation

6. In CI / CD what is continuous integration?
    a. Small changes to code are committed frequently often multiple times a day
    b. This is the process in which production code is updated
    c. Keeping local code up to date with all of the smaller changes and then committing to the primary repository all at once so that all the changes are included
    d. This keeps the code in a deployable state by ensuring that the code has been tested and validated

7. Which of the following are benefits of continuous integration? (Choose all that apply)

    a. Early detection of integration problems
    b. Eliminates the pre-release scramble
    c. Encourage good coding practices
    d. Automated scaling

8. How often should you be deploying to production with continuous deployment?
    a. There is no set standard
    b. Every week
    c. Every six months
    d. On every code commit

9. What is continuous delivery?
    a. Throwing code over the wall frequently
    b. Constantly merging code
    c. Always maintaining code in a deployable state
    d. Frequent deployments

10. What is one way to update invested parties of the status of the latest build?
    a. Notifications
    b. Integration
    c. Monitoring
    d. Webhooks

11. Which type of test is used to test the smallest unit of code?
    a. Acceptance
    b. Unit
    c. Smoke
    d. Regression

12. What is the definition of CI/CD
    a. This is a coding philosophy that allows teams to deliver tested, validated code that could be deployed at any time using small changes and frequent commits
    b. This is a DevOps process that allows code to be deployed to production daily
    c. This is a pipeline that pushed tested validated code to production frequently and in an automated manner
    d. This is a process in which code is integrated often and the code is manually tested and validated once the testing team has received notification that the code is ready to be tested