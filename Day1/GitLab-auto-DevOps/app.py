import argparse
parser = argparse.ArgumentParser(prog='main', formatter_class=lambda prog: argparse.HelpFormatter(prog, max_help_position=100, width=200))
parser.add_argument('name', help='Name parameter')
args = parser.parse_args()
print ('your name is ' + args.name)