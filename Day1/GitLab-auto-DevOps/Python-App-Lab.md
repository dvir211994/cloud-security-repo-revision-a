### Lab - Use GitLab auto DevOps to test a Python application

##### Objectives:

- Understand basic concepts of Continuous Integration
- Develop basic Python program
- Understand the importance of code testing and implementation

##### Steps:

1. Go to your GitLab account and create a repo for your application

2. Implement a simple Python program
    - Gets 1 input string argument (should limit to one)
    - The string should specify a name
    - The application will print to the screen (stdout) - "Hello, %arg1%"
    - The input string cannot be more than 12 characters and should be truncated if passes
    - Bonus: the application will get as input a sentence and will output the sentiment score of this sentence (can use textblob)

3. Write 3 test verifications to the app in a file called `tests.py`, for each test explain why you chose it
    - For example, a test to make that when given argument with 15 characters we expect it to be truncated to only the first 12

##### Submission:

- Submit the Python script you creted