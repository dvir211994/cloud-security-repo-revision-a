### Lab - Configure the ".gitlab-ci.yml" file in the repo where you created the Python app

##### Objectives:
- Create and configure a `.gitlab-ci.yml` to run GitLab Auto Devops
- Run tests and examine the results

##### Steps:

1. Configure the ".gitlab-ci.yml" on your repo to include CI testing (place it at the root of your repo)

2. Push a commit to the repo and verify that a CI pipeline is triggered

3. Verify the output of the testing and see that they are valid

4. Make a bug on the code on purpose and commit again, verify that pipeline failed and the testing caught the issue and printed the relevant log indication

##### Submission:
- Share the URL of your repo

Bonus challenge:
- Add a SAST to your tests, can be done through GitLab Auto Devops or implementing a usage of Bandit in your `tests.py` file
- Implement a calculation for the duration of each run, to get an understanding of your end to end pipeline total duration


Solution:
Content of the `.gitlab-ci.yml` file
aW1hZ2U6IHB5dGhvbjozCgp0ZXN0OgogICAgc2NyaXB0OgogICAgICAgIC0gcHl0aG9uMyB0ZXN0cy5weQ==