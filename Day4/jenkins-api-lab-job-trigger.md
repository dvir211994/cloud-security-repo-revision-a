### Lab: Triggering builds via the REST API and retrieving build statuses

##### Objectives:
- Acquiring API token from the Jenkins UI dashboard
- Running a curl command to trigger a specific job build in Jenkins
- Verify that the job indeed ran - through the dashboard, now we will see how we can check the build status of the build we triggered also in a automatic fashion using the API

##### Steps:

1. Acquire the API token from Jenkins UI

Solution:
MS4gTG9nIGluIHRvIEplbmtpbnMuCjIuIENsaWNrIHlvdSBuYW1lICh1cHBlci1yaWdodCBjb3JuZXIpLgozLiBDbGljayBDb25maWd1cmUgKGxlZnQtc2lkZSBtZW51KS4KNC4gVXNlICJBZGQgbmV3IFRva2VuIiBidXR0b24gdG8gZ2VuZXJhdGUgYSBuZXcgb25lIHRoZW4gbmFtZSBpdC4KNS4gWW91IG11c3QgY29weSB0aGUgdG9rZW4gd2hlbiB5b3UgZ2VuZXJhdGUgaXQgYXMgeW91IGNhbm5vdCB2aWV3IHRoZSB0b2tlbiBhZnRlcndhcmRzLgo2LiBSZXZva2Ugb2xkIHRva2VucyB3aGVuIG5vIGxvbmdlciBuZWVkZWQu

2. Use the token to build a job with parameters

Hint:
Y3VybCAtWCBQT1NUIC11IF9fX19fX19fOl9fX19fX19fX18gJ2h0dHA6Ly8ke0plbmtpbnNIb3N0fTo4MDgwL2pvYi9fX19fX19fX19fX18vYnVpbGRXaXRoUGFyYW1ldGVycz9fX19fX19fX19fPW1hc3Rlcic=

- Verify in the Jenkins UI that the build was successfully triggered

##### Submission:
- Submit the cURL request you sent to trigger the parameterized build