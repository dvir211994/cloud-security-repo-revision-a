### Demo - Create Jenkins build agents

##### Obejctives:
- Create build agents on remote AWS instances
- Integrate the agents and Jenkins using SSH
- Trigger a job through the agent instead of on the master Jenkins


##### Steps:

1. Create an EC2 instance (ubuntu 18 - t2.micro) for the build agent

2. SSH into the Jenkins agent

3. Install java on the machine and install a user dedicated for the remote builds

``` shell
# installing java
sudo su
apt-get update
apt-get install default-jre -y

# verify that Java is installed
java -version

# creating a user for the builds
adduser jenkins-agent
mkdir -p  /var/lib/jenkins
chown -R jenkins-agent /var/lib/jenkins

# allow password login ssh to jenkins agent
sudo sed -i 's/ChallengeResponseAuthentication no/ChallengeResponseAuthentication yes/g' /etc/ssh/sshd_config
sudo service sshd restart
```

4. SSH into the Jenkins master and, from master verify that you are able to SSH to the agent
``` shell
ssh jenkins-agent@${AgentIPAddress}
# enter the password you created for the agent user
```

5. Get back to the master server and generate SSH keys
``` shell
sudo su
docker exec -it $(docker ps -q) bash
cd ~
ssh-keygen -t rsa
# you should see two files added to you ~/.ssh/ folder - id_rsa and id_rsa.pub
# if needed, to escape out of the container press CTRL + P + Q
```

6. Copy the SSH keys to the agent server
``` shell
ssh-copy-id jenkins-agent@${AgentIPAddress}
```

7. Make sure that you are able to SSH without being prompted for a password from the master to the agent

8. Copy the content of your private RSA key `id_rsa` - for later use at the Jenkins using
``` shell
cat ~/.ssh/id_rsa
```

9. Browse to Jenkins UI `http://${JenkinsPublicIP}:8080`
    - Go to Manage Jenkins
    - Click Manage Nodes and Clouds
    - Click New Node in the left-hand menu
    - For Node name, enter ubuntu_agent
    - Select Permanent Agent

Set the following parameters for the agent
    - # of executors: 4
    - Remote root directory: /var/lib/jagent
    - Labels: ubuntu_agent
    - Usage: Only build jobs with label expressions matching this node
    - Launch method: Launch agents via SSH
    - Host: Enter the agent server's private/public IP address
    - Credentials: Add>Jenkins
        - Kind: SSH Username with private key
        - Description: ubuntu-agent
        - Username: jenkins-agent
        - Private Key: Enter the content of the id_rsa private key you copied earlier
        - Click Add

Once finished creating the credentials, on the credentials drop down select "jenkins-agent" and hit Save

10. Go to Nodes, and verify that you can see that new node and that Jenkins is able to initialize that node as an agent
