### Demo 2: Create a freestyle job with a GitHub repo

Objectives:
- Create a freestyle job with integration to a GitHub or GitLab repo

Steps:

1. Create a new freestyle job or edit a current one

2. Under "Configure" → "Source Code Management" → "Git" → specify any public repository that you would like to pull to the workspace to work on

3. In this demo we are not yet using GitHub credentials since we are fetching a public key that does not require authentication

4. Later down the road we will use private repositories that will require us to authenticate

5. Run the job and check the workspace, see that the repo was indeed fetched into the workspace