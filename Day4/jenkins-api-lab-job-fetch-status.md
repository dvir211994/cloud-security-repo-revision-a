### Lab: Triggering builds via the REST API and retrieving build statuses

##### Objectives:
- Verify that the parameterized job indeed ran using the API

##### Steps:

1. After triggering the job from the last lab, we now want to verify the result of our last build run in an automatic fashion, run a curl command to verify the result of the last build run

Hint:
Y3VybCAtdSBfX19fX19fXzpfX19fX19fX19fICdodHRwOi8vJHtKZW5raW5zSG9zdH06ODA4MC9qb2IvX19fX19fX19fX19fL2xhc3RCdWlsZC9hcGkvanNvbicgfCBqcQ0KDQojIFRoZSBKU09OIG91dHB1dCBwYXJzaW5nIGNhbiBhbHNvIGJlIGRvbmUgdmlhIFB5dGhvbiB1c2luZyB0aGUganNvbi50b29sIG1vZHVsZQ0KY3VybCAtdSBfX19fX19fXzpfX19fX19fX19fICdodHRwOi8vJHtKZW5raW5zSG9zdH06ODA4MC9qb2IvX19fX19fX19fX19fL2xhc3RCdWlsZC9hcGkvanNvbicgfCBweXRob24gLW1qc29uLnRvb2w=

- Parse the output using 'jq' or Python module 'json.tool' to show only the result of the last build

##### Submission:
- Submit the cURL request you sent to retrieve the last build status