### Lab 3: Create a freestyle job with parameters

##### Objectives:
- Configure a freestyle job to get input parameters

##### Steps:

1. Update the last freestyle job you worked on to enable input parameters

2. Under configuration, check "This project is parameterized"

3. Insert a new parameter called `Branch`

4. In the "Source Code Management" section, update the "Branches to build" to dynamically fetch the branch based on the parameter input we got
E.g - user initiated the job and gave 'master' as the input for the `Branch` parameter, the job should pull the repo in this specific branch

##### Submission:
- Submit the content of `config.xml` for the job you created
Hint:
Example for config.xml file location - if used docker volume in the container
L3Zhci9saWIvZG9ja2VyL3ZvbHVtZXMvamVua2luc19ob21lL19kYXRhL2pvYnMvdGVzdC1mcmVlc3R5bGUtam9iL2NvbmZpZy54bWw=

Bonus challenge:
- Fetch a private repository - notice that this will require you to create credetials to authenticate when pulling the repo
- Configure the job to run periodically, every month on the 15th
- Configure the job to clean the workspace before each build and keep 15 last build information