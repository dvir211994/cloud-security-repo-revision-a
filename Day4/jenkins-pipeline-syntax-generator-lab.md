### Lab: Using the Pipeline Syntax generator

##### Objectives:
- Working with the Pipeline Syntax generator
- Creating a pipeline job

##### Steps:

1. Create a pipeline job > configure

2. Select pipeline script from pipeline definition

3. Click on Pipeline syntax > snippet generator

4. Step > select Git > enter repo URL

5. Scroll down > Generate pipeline script

6. Go over the options in the generator and examine them, understand what are your possibilities when using this tool

7. Select a specific pipeline you would like to create, generate the script for it using the generator and create a new pipeline job

8. Copy the script from the generator into your pipeline script UI