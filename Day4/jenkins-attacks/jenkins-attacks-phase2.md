### Lab - Attacking Jenkins

##### Phase 2: After we have acquired login credentials to the Jenkins instance, we can use those in order to retrieve all the job folders for the build that were run on that Jenkins, in hope to find sensitive data in the console output

##### Objectives:
- Fetch the output of job builds to search for sensitive information

##### Steps:

1. Examine the code for `dump-jenkins-builds.py`

2. Understand how to run the tool using the command line and what parameters to pas

3. Run the tool and verify that you get back all the jobs folder to your workstation

Hint:
cHl0aG9uMyBkdW1wLWplbmtpbnMtYnVpbGRzLnB5IC11IF9fX19fX19fX19fXyAtcCBfX19fX19fX19fIC1vIC4gaHR0cDovL19fX19fX19fX19fX186ODA4MA==