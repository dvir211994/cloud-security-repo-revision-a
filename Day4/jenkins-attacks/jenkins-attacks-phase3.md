### Lab - Attacking Jenkins

##### Phase 3: Disable secured login feature, which will enable us to log in to the Jenkins UI without being prompted for credentials
* In the last phase we found the credentials for Jenkins, after this was acquired we can potentially create a new job which will post sensitive information like ENV variables, /etc/passwd contents and so on, this last phase assumes we were able to find information which led us to have SSH access to the Jenkins server

##### Objectives:
- Disable the secure login feature in Jenkins post exploitation

##### Steps:

1. SSH to the Jenkins server and stop the Jenkins service (the easiest way to do this is to stop the servlet container)

2. Go to `$JENKINS_HOME` in the file system and find `config.xml` file.

3. Open this file in the editor.

4. Look for the `<useSecurity>true</useSecurity>` element in this file.

5. Replace `true` with `false`

6. Remove the elements `authorizationStrategy` and `securityRealm`

7. Start Jenkins

8. When Jenkins comes back, it will be in an unsecured mode where everyone gets full access to the system.

9. If this is still not working, trying renaming or deleting `config.xml`.