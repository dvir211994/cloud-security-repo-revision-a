### Lab - Attacking Jenkins


#####  Phase 1: Finding Jenkins login credentials using dictionary attack and fetching sensitive data

##### Objectives:
- Find credentials of a Jenkins server
- Once credentials are found and Jenkins server is compromised
    - Use credentials to dump build results to search for sensitive data in output
    - Disable the secure login feature to Jenkins UI so anyone with network access can reach it

##### Steps:

1. Examine the code under `jenkins-auth-break.py`

2. Create a dictionary file for the optional passwords you want to test,
Bonus:
- Create a wordlist of all the possible phone combinations in Singapore (can use `crunch`)

* Example dictionary file can be found under `10-million-password-list-top-1000000.txt`

3. Edit the password list to have the password of the user you created, to do it from the terminal you can simply run `vim 10-million-password-list-top-1000000.txt` and then press ':' and enter the line number, for example go to line 150 and enter you password there

3. Run the tool downloaded in step 1, for example, supplying an explicit user name and providing a dictionary password list
Understand from viewing the source code, how you need to run the tool from the command line and which parameters to pass

Hint:
cHl0aG9uMyBqZW5raW5zLWF1dGgtYnJlYWsucHkgaHR0cDovL19fX19fX19fXzo4MDgwIC11IF9fX19fX19fXyAtUCAxMC1taWxsaW9uLXBhc3N3b3JkLWxpc3QtdG9wLTEwMDAwMDAudHh0DQo=

* note that this can take a few minutes depending on the number of workers configured inside the tool (line 85 in the `jenkins-auth-break.py`)