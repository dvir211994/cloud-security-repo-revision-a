### Demo - Creating a Consul cluster and registering a new service

##### Objectives:
- Create a Consul cluster with 3 agents
- Register a new service to Consul
- Query the cluster to get specific service instances
- Configure health checks

##### Steps:

1. Create 2 EC2 instances (Ubuntu 18, t2.micro)
    - 1x Consul cluster instance (will have multiple container cluster servers)
    - 1x Web service instance

2. SSH to the first Consul cluster servers, and run the following

##### Download and install Docker

``` shell
sudo su
curl -fsSL https://get.docker.com -o get-docker.sh
chmod +x get-docker.sh
./get-docker.sh
```

##### Run a containerized Consul server

```shell
# Here we run a detached container from "consul" image with name "Cluster-server-1"
# We make the UI accessible by binding to the external IP and exsposing port 8500 to the host
docker run -d --name=cluster-server-1 -p 8500:8500 -p 8600:8600 consul agent -dev -client=0.0.0.0 -bind=0.0.0.0
# We did not join this instance to a cluster, so it will create a new one.

# Now we get the internal IP of the "cluster-server-1" container, this will be needed to add the agents to the cluster
ConsulServerIP=$(docker inspect --format '{{ .NetworkSettings.IPAddress }}' cluster-server-1); echo $ConsulServerIP
```

3. Add additional servers to the cluster

##### Create 2 additional servers which will join the existing cluster server

``` shell
docker run -d --name=cluster-server-2 consul agent -dev -bind=0.0.0.0 -join=$ConsulServerIP
docker run -d --name=cluster-server-3 consul agent -dev -bind=0.0.0.0 -join=$ConsulServerIP
```

4. Go over the logs of our cluster leader and examine them to see the connections of the 2 new cluster members
``` shell
docker logs -f cluster-server-1
```

5. Now, let's simulate a failure of one of the nodes from the cluster
``` shell
# Stopping one of the cluster servers
docker stop cluster-server-3
# Viewing the current status of the cluster, you should expect to see that one of the 3 servers is not alive
docker exec cluster-server-1 consul members
# Get the container back up and verify that it has joined the cluster
docker start cluster-server-3
docker exec cluster-server-1 consul members
```

6. Access the Consul UI to view the cluster and services current status

``` shell
# Get the external IP of your Consul server
curl ifconfig.io
# Access your web browser at this address http://${PublicIP}:8500/
```

7. Examine the Consul UI and go over basic information shown

8. Register a new service to Consul, SSH to the Web service instance

##### Download and install Docker

``` shell
sudo su
curl -fsSL https://get.docker.com -o get-docker.sh
chmod +x get-docker.sh
./get-docker.sh
```

##### Set the IP of the Consul cluster instance
``` shell
# Get the value from the command you executed at step 6
ConsulServerIP='X.X.X.X'
```

##### Run a web service Nginx container

``` shell
docker run --name nginx -d -p 80:80 nginx
```

##### Register the service at the Consul cluster

``` shell
ExternalIP=$(curl ifconfig.io)
echo '{
  "ID": "WebService1",
  "Name": "WebService",
  "Address": "'${ExternalIP}'",
  "Port": 80
}' > WebService1.json

# Registering the service

curl -X PUT --data-binary @WebService1.json http://${ConsulServerIP}:8500/v1/agent/service/register
```

9. Browse to Consul UI and verify that you see the new service you just registered

10. Now let's see how we can query the Consul cluster to find instances under a specific service

``` shell
dig @$ConsulServerIP -p 8600 WebService.service.consul
```

11. Configuring a health check for our service

##### Deregister the previous service we created

``` shell
curl -X PUT http://$ConsulServerIP:8500/v1/agent/service/deregister/WebService1
```

##### Register the service again, this time with a health check

``` shell
ExternalIP=$(curl ifconfig.io)
echo '{
  "ID": "WebService1",
  "Name": "WebService",
  "Address": "'${ExternalIP}'",
  "Port": 80,
  "check": {
    "http": "http://'${ExternalIP}':80",
    "interval": "10s",
    "timeout": "1s"
   }
}' > WebService1.json

curl -X PUT --data-binary @WebService1.json http://$ConsulServerIP:8500/v1/agent/service/register
```

12. Browse to the Consul UI and verify that you see the new service and the configured health check

13. Stop the Nginx container to simulate a failure

``` shell
docker stop nginx
```

14. Verify in the UI you see that the service is not passing the health checks

15. Start the container back up and verify that you see it healthy in the Consul UI

``` shell
docker start nginx
```


Bonus challenge:

- Create additional health checks to verify that a specific service is running for example "docker"
- Create additional health checks to verify that a specific TCP port is listening
- Create an automation to create this whole architecture using EC2 user-data (run using the AWS CLI)

Hints:

U2VydmljZSBoZWFsdGggY2hlY2sgY29uZmlndXJhdGlvbgp7CiAgImlkIjogInNlcnZpY2UiLAogICJuYW1lIjogIkRvY2tlciBzZXJ2aWNlIHN0YXR1cyIsCiAgImFyZ3MiOiBbInNlcnZpY2UiLCAiZG9ja2VyIiwgInN0YXR1cyJdLAogICJpbnRlcnZhbCI6ICI2MHMiCn0KVENQIHBvcnQgaGVhbHRoIGNoZWNrIGNvbmZpZ3VyYXRpb24KewogICJpZCI6ICJ0Y3AiLAogICJuYW1lIjogIlRDUCBvbiBwb3J0IDgwIiwKICAidGNwIjogImxvY2FsaG9zdDo4MCIsCiAgImludGVydmFsIjogIjEwcyIsCiAgInRpbWVvdXQiOiAiMXMiCn0=