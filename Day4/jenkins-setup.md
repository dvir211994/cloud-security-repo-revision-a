### Create an EC2 instance (t2.micro) for Jenkins

##### Objectives:
- Create an EC2 instance, install and configure Jenkins on it

##### Steps:

1. Create an EC2 instance on AWS

2. Install Docker on that instance

3. Install and configure Jenkins

``` shell
docker run -p 8080:8080 -p 50000:50000 -d -v jenkins_home:/var/jenkins_home jenkins/jenkins:latest
```

4. Browse to the external address of your Jenkins instance at port 8080

5. You will be prompted with the screen that shows that Jenkins is booting, once it is done, the setup wizard will ask you for the access key, to verify that you have access to the instance file system and actually own the machine

6. After the container has finished to spin up, run `docker logs -f $(docker ps -q)`

7. This will give us the docker logs of the container, there we should see the access key and insert it in the setup wizard