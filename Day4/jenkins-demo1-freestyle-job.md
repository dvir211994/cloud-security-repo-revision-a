### Demo 1: Create a simple Freestyle Job

1. Create a sample freestyle project with "Execute Shell" step - this job will only output the result of a Shell command

2. Go the the UI and see that there are no files under the last build workspace

3. Edit the job to make it write the output to a file

4. Go to the UI or the file system and now show that this file was exist in the workspace

In the UI its under the job options → "Workspace"
In the filesystem its `/var/lib/docker/volumes/%Docker-Volume-Name%/_data/workspace/%Job-Name%`

5. Let's take a look on the "Console output" to see the logs of the run

In the UI its under the job options → "Console Output"
In the filesystem its `/var/lib/docker/volumes/%Docker-Volume-Name%/_data/jobs/%Job-Name%/builds/1/log`

6. Let's wipe the workspace before the build starts

7. Under "Configure" check the "Delete workspace before build starts" box