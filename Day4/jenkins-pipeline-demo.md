### Demo: Using the Pipeline Syntax generator

1. Create a pipeline job > configure

2. Select pipeline script from pipeline definition

3. Click on Pipeline syntax > snippet generator

4. Step > select Git > enter repo URL

5. Scroll down > Generate pipeline script

6. Copy the script into your pipeline script UI


### Demo: Pipeline job using Jenkins stored credentials

Objective:
- using Jenkins stored credentials in order to use secrets that we don't want hard-codded in our code

##### Steps:

1. Create a pipeline job and see that the job fails since it is not familiar with the secret called "jenkins-secret"

2. Create a secret in Jenkins which uses the ID "jenkins-secret"

3. Run the job again and see that now the job is successful and we get the console output

4. Verify that the secret is not being logged in the console output

```groovy
pipeline {

agent any

environment {
    SECRET=credentials('jenkins-secret')
}
stages {
    stage('Build') {
        steps {
            echo "${env.SECRET}"
            }
        }
    }
}
```