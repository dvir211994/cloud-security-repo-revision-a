### Demo - deploy the ELK stack on AWS EC2 instances

##### Objectives:
- Install a single-node for ELK stack
- Install different beats and configure them to ship logs to the ELK instance
- Work with Kibana UI


##### Steps:

1. Create 2 EC2 instances (t2.large - ELK, t2.micro - client)

2. Deploying ELK stack on a single-node EC2 instance (you can see reference below)

3. Security hardening on the instance using Ansible playbook

4. Installing and configuring Kibana to an Elasticsearch Cluster

5. Shipping Logs with Metricbeat (AWS EC2 data), Packetbeat (Network traffic monitoring) and Heartbeat (Checking the availability of our web app)

6. Install and configure Nginx, configure Heartbeat to watch the Nginx web page

7. Walk-around the Kibana UI and analyzing Data

https://www.elastic.co/guide/en/beats/packetbeat/current/kibana-queries-filters.html

8. Creating a Kibana visualisation and Kibana dashboard



You can find reference for installing the required components below

##### Install Java

    sudo apt-get update
    
    sudo apt-get install openjdk-8-jdk

##### 1. Download and install public signing key 

    wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -

##### 2. Install apt-transport-https package

    sudo apt-get install apt-transport-https -y

##### 3. Save directory definitions

    echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list

##### 4. Update and Install elasticsearch

    sudo apt-get update && sudo apt-get install elasticsearch && sudo apt-get install logstash && sudo apt-get install kibana

##### 5. configure elasticsearch

    sudo su
    nano /etc/elasticsearch/elasticsearch.yml

    change cluster name
    cluster.name: awstg-elk  

    give the cluster a descriptive name
    node.name: aws-node

    change network binding
    network.host: 0.0.0.0  

    setup discovery.type as single node
    discovery.type: single-node

##### 6. Start Elasticsearch service

    sudo systemctl start elasticsearch

##### 7. validate Elasticsearch cluster health

    curl -XGET http://localhost:9200/_cluster/health?pretty

##### 8. configure kibana
    
    nano /etc/kibana/kibana.yml

    uncomment server.port
    server.port: 5601

    change server.host
    server.host: "0.0.0.0"
    
    change server.name
    server.name: "awstg-kibana"
    
    uncomment elasticsearch.host
    elasticsearch.hosts: ["http://localhost:9200"]
    
##### 9. start Kibana service

    systemctl start kibana
    
##### 10. enable elasticsearch and kibana

    systemctl enable elasticsearch
    systemctl enable kibana