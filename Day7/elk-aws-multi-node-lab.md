### Lab - deploy a multi node deployment of ELK stack on AWS, using Ansible and Terraform


##### Objectives:
- Work with IaC automation tools to automate the whole process
- Apply security hardening using Ansible
- Configure Elastic beats to report to the ELK stack to have observability over your network


##### Steps:

1. Create 2 EC2 instances using Terraform to create the infrastructure (t2.large - ELK, t2.micro - client)

2. Deploy ELK stack using Ansible
    - Configure different Roles,
        - Common - to run on all instances, including security hardening
        - Elastic - to run on ELK host
        - Kibana - to run on ELK host
        - Nginx - to run on client host

2. Create a sample application in Python that generates logs into a file, and apply that in Ansible

3. On the client host, install and configure Filebeat to ship log files to ES

4. On the client host, install and configure Metricbeat to ship metrics to ES

5. Go to Kibana dashboard and verify you see your data


Bonus challenge:

- Run the whole process automated using Terraform and Ansible

Hint:
U3VnZ2VzdGVkIHBsYW4gb2YgYWN0aW9uOgoKQ3JlYXRlIGEgQmFzaCBzY3JpcHQgd2hpY2ggZG9lcyB0aGUgZm9sbG93aW5nLAoKMS4gUnVuIHRlcnJhZm9ybQogICAgLSBUZXJyYWZvcm0gY3JlYXRlcyAyIEVDMiBpbnN0YW5jZXMgLSBFTEsgYW5kIEZsYXNrCiAgICAtIENyZWF0ZSB0aGUgaW5zdGFuY2VzIHdpdGggYW4gaW5kaWNhdGl2ZSB0YWdnaW5nIHNvIHlvdSBjYW4gbGF0ZXIgcmVmZXJlbmNlIHRob3NlIHNwZWNpZmljIGluc3RhbmNlcwoKMi4gUnVuIEFuc2libGUKICAgIC0gQW5zaWJsZSB3aWxsIHJ1biBvbiBob3N0cyB1c2luZyBkeW5hbWljIGludmVudG9yeSBmaWx0ZXJpbmcgYnkgQVdTIHRhZ3MKICAgIC0gQW5zaWJsZSB3aWxsIGRlcGxveSB0byBlYWNoIGluc3RhbmNlIGFjY29yZGluZyB0byBoaXMgdGFnLCBmb3IgZXhhbXBsZSwgdGFnICJTZXJ2aWNlIjogIkVMSyIgd2lsbCBydW4gRUxLIHBsYXlib29rIG9uIHRoYXQgaG9zdA