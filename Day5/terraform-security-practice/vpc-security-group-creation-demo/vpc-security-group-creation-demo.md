### Demo - VPC and security groups creation using Terraform

##### Objectives:
- Create a new VPC with all of its required components
- Verify in AWS Console that the VPC was seccesfuly created

##### Steps:

1. Create a Terraform project which will create a new VPC in a given region
* The region can also be configured in the variables.tf file to avoid hard-coding it

2. Configure security group rules other components of the VPC

3. Run `Terraform plan` and verify the plan

4. After examining the plan and confirming it, run `Terraform apply`

5. Verify that the VPC was created through AWS Console