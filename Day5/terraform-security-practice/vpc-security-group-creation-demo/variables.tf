variable "vpc_cidr" {
  default = "10.1.0.0/16"
}

variable "public_cidrs" {
  type = "list"
  default = ["10.1.10.0/24","10.1.11.0/24"]
}

variable "private_cidrs" {
  type = "list"
  default = ["10.1.12.0/24","10.1.13.0/24"]
}