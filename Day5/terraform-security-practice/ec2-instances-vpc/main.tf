provider "aws" {
  region = "us-east-1"
  profile = "dvir-personal"
}

# To get the latest Centos7 AMI
data "aws_ami" "ubuntu" {
  owners      = ["099720109477"]
  most_recent = true

  filter {
    name   = "name"
    values = ["*ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-20200908*"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }

  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }
}

resource "aws_key_pair" "mytest-key" {
  key_name   = "my-test-terraform-key"
  public_key = "${file(var.my_public_key)}"
}

resource "aws_instance" "test_instance" {
  count                  = "${var.instance_count}"
  ami                    = "${data.aws_ami.ubuntu.id}"
  instance_type          = "${var.instance_type}"
  key_name               = "${aws_key_pair.mytest-key.id}"
  tags = {Name = "my-test-server.${count.index + 1}"}
}

