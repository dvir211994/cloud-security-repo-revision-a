### Lab - Create an EC2 instance using Terraform

##### Objectives:
- Create an EC2 instance, with secure connection configuration
- Create your own ephemeral key pair and use it to connect to the instance
- Make sure that IP whitelist is configured and SSH is allowed only from your external IP address


Prerequisites:
- Create an RSA key pair (c3NoLWtleWdlbiAtdCByc2E=)
- Configure the variabless.tf file to include the path for the public key you just created

##### Steps:

1. Create a Terraform project which will create an EC2 instance on AWS (in a specific region)

* The region selection should be part of the variables.tf file

2. Using the AWS Console, find an AMI (Amazon Machine Image) you want to use for the instance creation
* Make sure that the AMI you want to for the machine exists in the region you are deploying (AMI IDs are different for each region)

3. Run `Terraform plan` and make sure that all your configurations are in place

4. Run `Terraform apply` and run the deployment

5. Make sure that you can see the instance in your AWS Console on the specific region you deployed to

6. Connect to the instance using the key pair you created

##### Submission:
- Submit the Terraform project folder you created (zipped)

Bonus challenge:
- Add a "user data" script to the instance to create a cron job, that each hour exactly it will print to a log on /tmp/log.log "KooKoo, it has been a hour"
- Replace the Ubuntu image with a Centos image
- Add another output to post the VPC and Public IP of the EC2 instance that was created (for each one of them if multiple instances were created)