variable "my_public_key" {
  default = "/tmp/tf-test/id_rsa.pub"
}
variable "instance_type" {
  default = "t2.micro"
}
variable "instance_count" {
  default = "1"
}
variable "security_group" {
  default = "sg-735a9d3b"
}