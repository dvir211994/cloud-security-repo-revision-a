### Setup - creating a local Ubuntu server for local Ansible tests using SSH

##### Steps:

1. Update the Dockerfile with your public SSH key

2. Create the docker image using `docker build -t local-ssh-container .`
* You can run this command from the folder where the Docker file exists or specify its location

3. Run `docker images` and verify that the new image got created

4. Run the container
`docker run -d -p 2222:22 local-ssh-container`

5. Try to SSH into the container using SSH key authentication
`ssh root@localhost -p 2222 -i .ssh/id_rsa`

6. Update the docker Hosts file to include a non standard SSH port, for example

[local]
localhost:2222