### Demo: Creating our first resource using Terraform - S3 bucket

##### Objectives:
- Creating an AWS resource using Terraform, S3 bucket


##### Steps:
  
1. Create 2 files `s3-bucket.tf` and `variables.tf` 

2. Run `terraform init` to initialize the working folder

3. Run `terraform plan` to verify we are okay with the changes before we apply them

4. Run `terraform apply` to run the project and create our resource

5. Log in to AWS console and verify that you see the newly created S3 bucket

Bonus challenge:

- Create a Terraform plan to create 2 EC2 instances behind an LB (create the instances with a specific tag that you can reference later)
- Use Ansible to dynamically query your AWS inventory based on the tag you created and have an inventory based on those two instances you created earlier
- Run an Ansible playbook which will install Python and Flask on those instances, and copy the Flask application to the instances and make them run
- Reach the LB on port 8080 and see that each time you are directed to a different backend server