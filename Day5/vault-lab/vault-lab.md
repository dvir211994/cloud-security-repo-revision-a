### Lab - using encryption on sensitive variables using Ansible Vault

##### Objectives:

- Create a sample playbook that will display the secret password to stdout
- Specify the Vault secret variable file in the `vars_files` on `app.yml`
- Run the playbook specifying the vault password

##### Steps:

1. Create a secret variable file using the `ansible-vault` command line tool

Solution:
YW5zaWJsZS12YXVsdCBjcmVhdGUgdmFycy9zZWNyZXQtdmFycy55bWw=

* The tool will prompt us to insert a password which will encrypt the data inside the file

* Once we entered the password, the secret var file will be opened in your default text editor

You can edit the content of the secrets file as follows:

```yaml
db_password: "myStrongPassword123"
```

if we want to later on edit the secrets file, we can do so by

``` shell
ansible-vault edit vars/secret-vars.yml
```

2. Create a playbook which will output the plain text value of the secret variable you created

3. Run the playbook, using the Vault

``` shell
ansible-playbook app.yml --ask-vault-pass
```

* If you did not create a secrets file of your own and decided to use the one in the repo, the password is 'password'
* Note that if you run the playbook without --ask-vault-pass it will fail - which is also the power of Ansible vault, being that you can not run the playbook which has secrets inside it if you do not specify the password

##### Submission:
- Submit the Ansible playbook run output, showing the print of the plain text value for the secret variable you configured

Bonus challenge:
- Create a new playbook file and encrypt the playbook itself using ansible-vault
- Run the playbook with passing a file containing the password instead of typing the password in the prompt