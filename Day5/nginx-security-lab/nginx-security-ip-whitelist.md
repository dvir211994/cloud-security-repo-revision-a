### nginx security lab - configure whitelist based on IP addresses

##### Objectives:
- Secure access to your Nginx server by restricting access based on origin IP

##### Steps:
1. Create or use an existing EC2 instance (t2.micro)

2. Install docker on that instance

Solution:
Y3VybCAtZnNTTCBodHRwczovL2dldC5kb2NrZXIuY29tIC1vIGdldC1kb2NrZXIuc2gKc3VkbyBzaCBnZXQtZG9ja2VyLnNo

3. Run an nginx container on that instance on ports 80 and 443

Hint:
ZG9ja2VyIHJ1biAtcCBfX19fXzo4MCAtcCA0NDM6X19fX19fXyAtZCBuZ2lueA==

4. Run a local cURL on the instance to verify that nginx is up

Solution:
Y3VybCBsb2NhbGhvc3Q=

* You should see an nginx greeting page

5. Exec into the docker container to get shell access

Hint:
IyBGaW5kIHlvdXIgY3VycmVudCBydW5uaW5nIGNvbnRhaW5lcnMgdXNpbmcNCmRvY2tlciBfX19fX19fX18NCg0KIyBFeGVjIGludG8gdGhhdCBjb250YWluZXIgdXNpbmcgdGhlIGNvbnRhaW5lciBJRCBvciBuYW1lDQpkb2NrZXIgZXhlYyAtaXQgX19fX19fX19fXyBiYXNoDQoNCiMgSW4gYSBzaG9ydGVyIHZlcnNpb24sIGlmIHlvdSBoYXZlIG9ubHkgMSBjb250YWluZXIgcnVubmluZyBvbiB5b3VyIGluc3RhbmNlDQojIGRvY2tlciBleGVjIC1pdCAkKGRvY2tlciBwcyAtcSkgYmFzaA0K

6. Edit your main nginx configuration file

``` shell
vim /etc/nginx/conf.d/default.conf

# If vim is not installed you can install it using
# apt-get update && apt-get install vim -y
# yum update && yum install vim -y
```

7. Add a new location to the Nginx configuration file and add your public IP

Hint:
bG9jYXRpb24gL3ZpcC8gew0KCXJvb3QgICAvdXNyL3NoYXJlL25naW54L2h0bWw7DQoJaW5kZXggIGluZGV4Lmh0bWwgaW5kZXguaHRtOw0KCWFsbG93ICBfX19fX19fX19fXy8zMjsNCglkZW55ICAgYWxsOw0KfQ0K

* You can find your public IP by running `curl ifconfig.io`

8. Create a new folder under the following location and create an index.html file

```groovy
mkdir -p /usr/share/nginx/html/vip
touch /usr/share/nginx/html/vip/index.html
```

9. Create a sample html file for our VIP location, so we can have an indication that we have access to the new site when reaching it

```html
<!DOCTYPE html>
<html>
<head>
<title>Welcome to VIP nginx!</title>
</head>
<body>
<h1>Welcome to VIP nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>
<p><em>Thank you for joining the VIP club.</em></p>
</body>
</html>
```

10. Since we change the main configuration file we will need to restart nginx, run this from the container

Solution:
bmdpbnggLXMgcmVsb2Fk

11. Go to your browser and try to reach the nginx server at port 80

First try accessing the root level `/`

Then try accessing the VIP level `/vip`

##### Submission:
- Submit the external IP of your of the nginx instance
