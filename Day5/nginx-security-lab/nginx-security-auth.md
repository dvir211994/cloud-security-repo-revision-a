### Lab - Configure password authentication in Nginx

1. Log in to your EC2 instance

2. Exec into the nginx container
`docker exec -it $(docker ps -q) bash`

3. Install apache utils so we can use `htpasswd`

``` shell
apt-get install apache2-utils
```

* note that nginx does not come with a tool to create password files, so in our case we are using a tool that is included in Apache

4. Create an `htpasswd` password file under the Nginx folder

Solution:
aHRwYXNzd2QgLWMgL2V0Yy9uZ2lueC9wYXNzd29yZHMgYWRtaW4=
* Running this will prompt us to add a password to the user

htpasswd extras:

- If we want to add a new user on top of the existing one we will run:
aHRwYXNzd2QgL2V0Yy9uZ2lueC9wYXNzd29yZHMgam9obg==

* Note that the `-c` is ommited, since it is used to create the file, if we run the command again with the '-c' flag it will override the existing file

- If we want to delete a user from the file we will run:
aHRwYXNzd2QgLUQgL2V0Yy9uZ2lueC9wYXNzd29yZHMgdXNlcjI=

5. Change the owner of the `htpasswd` file you created, to be the user that nginx is using

``` shell
chown www-data /etc/nginx/passwords
# Adding permissions so our root user has permissions for the file
chmod 600 /etc/nginx/passwords
```

6. Configure nginx to use the password file in our website

Hint:
YXV0aF9iYXNpYyAiQXV0aGVudGljYXRpb24gaXMgcmVxdWlyZWQsIHBsZWFzZSBsb2dpbiI7DQphdXRoX2Jhc2ljX3VzZXJfZmlsZSAvZXRjL25naW54L19fX19fX19fX19fOw==

7. Reload the nginx service

``` shell
nginx -s reload
```

8. From your browser, access the Nginx public IP, and verify that you are prompted to enter credentials