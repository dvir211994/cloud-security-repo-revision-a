### Lab - Create and configure an SSL certificate on Nginx

##### Objectives:
- Create and configure an SSL certificate on Nginx

##### Steps:

1. Install the Let's Encrypt certificates utility

``` shell
apt-get install -y certbot python-certbot-nginx
```

2. Create a DNS name A record
    * You can create a free one here https://www.noip.com
    * The A record should direct traffic to the IP address of your EC2 instance

3. Update the dns name you created in your nginx configuration file

Hint:
c2VydmVyIHsNCiAgICBsaXN0ZW4gICAgICAgODA7DQogICAgbGlzdGVuICBbOjpdOjgwOw0KICAgIHNlcnZlcl9uYW1lICBfX19fX19fX187

4. Browse to your instance using the DNS name and see that you are able to reach it

5. Run the certbot to acquire a certificate from Let's Encrypt

Solution:
Y2VydGJvdCBjZXJ0b25seSAtLW5naW54

6. Verify that the output tells you that the certificate was successfully generated and configured

7. Configure the new certificate in your nginx configurations using the certbot

``` shell
certbot install --nginx
```

8. Access your website using the DNS name and see that your website is using SSL

##### Submission:
- Submit the external IP address of your nginx server