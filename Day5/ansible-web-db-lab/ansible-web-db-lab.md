### Lab - Deploying a web server and a DB server on AWS

##### Objectives:
- Using Ansible to deploy to 2 different instances on AWS
- Create a playbook for deploying a web server and a DB server

##### Steps:

1. Create 2 EC2 instances (t2.micro)

2. Create a sample hosts file, with 2 groups

``` txt
[webservers]
3.95.6.26

[dbservers]
18.207.217.3
```

3. Create a playbook to deploy Apache2 on the web server

Hint:
LS0tDQojIENvbW1vbiBNb2R1bGVzIFBsYXlib29rDQojDQoNCi0gbmFtZTogQ29tbW9uIE1vZHVsZXMgUGxheWJvb2sNCiAgYmVjb21lOiB5ZXMNCiAgaG9zdHM6IF9fX19fX19fX19fDQoNCiAgdGFza3M6DQogICAtIG5hbWU6IFVwZ3JhZGUgYWxsIGFwdCBwYWNrYWdlcw0KICAgICBhcHQ6IHVwZ3JhZGU9ZGlzdCBmb3JjZV9hcHRfZ2V0PXllcw0KICAgLSBuYW1lOiBJbnN0YWxsIGFwYWNoZTIgb24gd2Vic2VydmVycw0KICAgICBhcHQ6DQogICAgICBuYW1lOiBhcGFjaGUyDQogICAgICBzdGF0ZTogX19fX19fX19fX19fDQogICAtIG5hbWU6IGRvd25sb2FkIHNhbXBsZSBpbmRleCBodG1sIGZpbGUNCiAgICAgZ2V0X3VybDoNCiAgICAgIHVybDogaHR0cHM6Ly9naXN0LmdpdGh1YnVzZXJjb250ZW50LmNvbS9uZmFycmluZy8xMTEzMjkyL3Jhdy9kMDUyNjMwYWY5NDViZDkzNDhiZTFiOTFlZjQ4ZGI4OThmNTEyYmNhL2luZGV4Lmh0bWwNCiAgICAgIGRlc3Q6IC92YXIvd3d3L2h0bWwvaW5kZXguaHRtbA0KICAgICAgbW9kZTogMDQ0MA0KICAgLSBuYW1lOiBTdGFydCB0aGUgYXBhY2hlMiBzZXJ2aWNlDQogICAgIHNlcnZpY2U6DQogICAgICBuYW1lOiBhcGFjaGUyDQogICAgICBzdGF0ZTogX19fX19fX19fDQogICAgICBlbmFibGVkOiBfX19fX19fX19fDQo=

* Please note that there is a deliberate error on one of the tasks in the playbook above, please deploy it yourself, find the issue when trying to access the web page from your browser, fix the issue in the server itself or redeploy using Ansible levereging its idempotence (recommended)

4. Create a playbook to deploy the DB server

Hint:
LS0tDQoNCi0gbmFtZTogREIgc2VydmVyIHBsYXlib29rDQogIGJlY29tZTogeWVzDQogIGhvc3RzOiBfX19fX19fX19fXw0KDQogIHRhc2tzOg0KICAgLSBuYW1lOiBBZGQgdGhlICdkYmEnIHVzZXIgdG8gdGhlIGRic2VydmVycw0KICAgICB1c2VyOg0KICAgICAgIG5hbWU6IF9fX19fX19fX18NCiAgIC0gbmFtZTogQ29weSByZXF1aXJlZCBEQkEgZmlsZXMNCiAgICAgY29weToNCiAgICAgIHNyYzogL3RtcC9jaGlub29rLmRiDQogICAgICBkZXN0OiAvaG9tZS9kYmEvDQogICAgICBvd25lcjogZGJhDQogICAgICBncm91cDogZGJhDQogICAgICBtb2RlOiAwNjAw

5. Verify that before running the deploys you are able to access the instances
Hint:
You can do so by running the ping module on specific subsets of the hosts file
Hint:
YW5zaWJsZSAnd2Vic2VydmVycywgZGJzZXJ2ZXJzJyAtbSBwaW5nIC1pIGhvc3Rz

6. Run the deploys for each one of the groups and verify that the output is successful

7. Access the web server public IP from your browser or terminal and verify that you get the index html file we uploaded from the playbook in step 3

Bonus challenge:
- Instaed of running each playbook in a different command, find a way to serially run both of the playbooks
- Update the index.html file to include the hostname and IP address of the EC2 instance that it resides in