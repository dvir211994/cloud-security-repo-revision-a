### Lab - Nginx Load balancing

##### Objectives:
- Run the docker-compose and create an architecture with nginx as the LB
- Sping up 2 containers running nginx as backend instances
- Browse to the web port the nginx servers are listening on and verify that each time you refresh you get to a different backend container

##### Steps:

1. Go over the code of the Dockerfile(s) and understand the way it works

2. Feel free to tweak any configuration you want in the Docker files or python app

3. Open the terminal and insert the following command
docker-compose up

4. This will spin up the docker containers for: "web-server1", "web-server2", "nginx-server"

5. Run `docker ps` and verify that all the containers are up and running

6. Open your browser and go to http://localhost:8080

7. Refresh the page and make sure that each time you are presented with a message from a different backend server 

Notes:
- 60% of the traffic will be routed to web-server1 and 40% will go to web-server2
- To cancel and kill the containers, you can hit `CTRL + C` in the terminal and it will stop the application and destroy the created containers

Bonus challenge:
- Update the configuration so that 30% of the traffic will be routed to web-server1 and 70% will go to web-server
- Run the orchestration in detached mode
- Create another service called watch dog, this service will examine the health of the Nginx container, checking port 8080 to verify that the web content is served as expected.
The watchdog will perform this check every 10 seconds and log it to a log file inside the container, make sure to use persistent volumes to not lose these precious logs