### Lab - LAMP on Ubuntu 18.04

##### Objectives:
- Use Ansible to install LAMP stack on a single Ubuntu machine


##### Steps:

1. Edit the vars/default.yml file to (if you wish, also works out of the box)

Explanation for each variable in the variables file:
```yml
---
mysql_root_password: "the password for the MySQL root account."
app_user: "a remote non-root user on the Ansible host that will own the application files."
http_host: "your domain name"
http_conf: "the name of the configuration file that will be created within Apache"
http_port: "HTTP port, default is 80"
disable_default: "whether or not to disable the default Apache website. When set to true, your new virtualhost should be used as default website. Default is true"
```

2. Run the ansible-playbook command

```shell
# ansible-playbook -l [target] -i [inventory file] -u [remote user] playbook.yml
# Note that using the '-l' flag you can specify a subset of server/s using an alias
ansible-playbook -l server1 -i hosts -u ubuntu playbook.yml
```

3. Go to the external IP address of your target server and browse to it on port 80, make sure you see "Apache2 Ubuntu Default Page"


Bonus challenge:
- Run the playbook on 2 different servers and find an automatic way to populate the private IP addresses of both (for configuration)
    - so for example, you created 2 servers, web and db, when the web db is booted it needs to know in the configuration what is the IP of the DB server he is going to be working with