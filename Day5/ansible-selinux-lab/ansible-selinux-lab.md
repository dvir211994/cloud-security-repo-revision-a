### Lab - Using Ansible playbooks to apply security configuration in our cloud infrastructure

##### Objectives:

- Create 2 EC2 instances on AWS (t2.micro, ubuntu)
  1x - webserver (no need to actually install any web application on it)
  1x- DB server (no need to actually install any DB application on it)
- Create an inventory file with the remote targets addresses (DNS/IP)
group 'webservers' - should include the web server instance
group 'dbservers' - should include the DB server instance
- Create 3 playbooks
  1 - To run on the 'webservers' group, ensuring ports 80, 22 are closed
  2 - To run on the 'dbservers' group, ensuring ports 5432, 22 are closed
  3 - On all targets, enable SELinux
  as a hint, you can take a look at the 'firewalld' and 'selinux' modules
- Run the playbook against, each playbook to the relevant target group

##### Steps:

1. Create 2 t2.micro EC2 instances in AWS in a desired region using the AWS CLI
    * Tag the instances in a way you can differentiate between the DB instances and the Webserver instances
    * For example, you can set, tag "Purpose": "Webserver", or "Purpose": "Database"

2. Run an AWS CLI command which will fetch the public IP addresses of those instances via tags filtering

3. Configure a hosts file which will include 2 groups: "webservers", "dbservers"

4. Create a playbook to make sure that ports 80, 22 are the `only` open ports on webservers

Hint (Update occurences of 'CHANGE_ME'):
IC0tLQotIG5hbWU6IHdlYnNlcnZlciBmaXJld2FsbCBydWxlcwogIGhvc3RzOiB3ZWJzZXJ2ZXJzCiAgYmVjb21lOiBDSEFOR0VfTUUKCiAgdGFza3M6CiAgIC0gbmFtZTogc3NoIGZpcmV3YWxsIHJ1bGVzCiAgICAgZmlyZXdhbGxkOgogICAgICBwZXJtYW5lbnQ6IENIQU5HRV9NRQogICAgICBzdGF0ZTogZW5hYmxlZAogICAgICBpbW1lZGlhdGU6IENIQU5HRV9NRQogICAgICBzZXJ2aWNlOiBzc2gKCiAgIC0gbmFtZTogaHR0cCBmaXJld2FsbCBydWxlcyAKICAgICBmaXJld2FsbGQ6CiAgICAgIHBlcm1hbmVudDogeWVzCiAgICAgIHN0YXRlOiBDSEFOR0VfTUUKICAgICAgaW1tZWRpYXRlOiB5ZXMKICAgICAgc2VydmljZTogQ0hBTkdFX01FCg==

5. Create a playbook to make sure that ports 5432, 22 are the only open ports on dbservers

Hint (Update occurences of 'CHANGE_ME'):
IC0gbmFtZTogZGIgZmlyZXdhbGwgcnVsZXMKICBob3N0czogZGJzZXJ2ZXJzCiAgYmVjb21lOiB5ZXMKCiAgdGFza3M6CiAgIC0gbmFtZTogc3NoIGZpcmV3YWxsIHJ1bGVzCiAgICAgZmlyZXdhbGxkOgogICAgICBwZXJtYW5lbnQ6IHllcwogICAgICBzdGF0ZTogQ0hBTkdFX01FCiAgICAgIGltbWVkaWF0ZTogeWVzCiAgICAgIHNlcnZpY2U6IENIQU5HRV9NRQoKICAgLSBuYW1lOiBwb3N0Z3Jlc3FsIGZpcmV3YWxsIHJ1bGVzIAogICAgIGZpcmV3YWxsZDoKICAgICAgcGVybWFuZW50OiB5ZXMKICAgICAgc3RhdGU6IENIQU5HRV9NRQogICAgICBpbW1lZGlhdGU6IHllcwogICAgICBzZXJ2aWNlOiBDSEFOR0VfTUU=

6. Create a playbook to enable SELinux on both groups

Hint (Update occurences of 'CHANGE_ME'):
LSBuYW1lOiBTRUxpbnV4CiAgaG9zdHM6IGFsbAogIGJlY29tZTogeWVzCgogIHRhc2tzOgogICAtIG5hbWU6IEVuYWJsZSBTRUxpbnV4CiAgICAgc2VsaW51eDoKICAgICAgc3RhdGU6IENIQU5HRV9NRQogICAgICBwb2xpY3k6IHRhcmdldGVk

Bonus challenge:

- Run the playbook only on servers in your AWS account that have a name tag called "staging" (don't forget to create those tags while creating the instances, or assigning it to them post creation)
* In order to do that you will need to use dynamic inventory, based on AWS tags