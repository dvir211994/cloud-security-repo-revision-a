### Demo: Using S3 bucket to serve as a backend to store the Terraform state

##### Objectives:
- Save the Terraform state in a backend (S3 bucket)

##### Steps:

1. Create an S3 bucket to serve the backend Terraform state file
* Make sure that the AWS user being used, has read/write permissions

2. Create a new folder project, inside it create a file called backend.tf

3. Run `terraform init`

3. Verify that you see in the output that Terraform has successfully configured the S3 backend bucket