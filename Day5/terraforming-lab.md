### Lab: Run a terraforming script to get current running objects of our EC2 instances in AWS

##### Objectives:
- Work with the Terraforming tool to export existing AWS resources

##### Steps:

1. Install the prerequisites

    A) Donwload the Terraforming tool
    https://github.com/dtan4/terraforming

    B) Install Ruby on your workstation (or VM)
    C) Configure AWS credentials, can be done via:
        A) ENV variables
            export AWS_ACCESS_KEY_ID=XXXXXXXXXXXXXXXXXXXX
            export AWS_SECRET_ACCESS_KEY=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
            export AWS_REGION=xx-yyyy-0
        B) Passing an argument of a current existing AWS profile in `~/.aws/credentials`

2. Install Terraforming using Ruby gems

Solution:
IyBJbnN0YWxsaW5nIFRlcnJhZm9ybWluZywgcnVieSBpcyBuZWVkZWQgaW4gb3JkZXIgdG8gaW5zdGFsbCBUZXJyYWZvcm1pbmcKc3VkbyBnZW0gaW5zdGFsbCB0ZXJyYWZvcm1pbmcK

3. Run Terraforming against your EC2 and S3 resources on AWS (specifying your AWS profile in `~/.aws/credentials`)

Hint:
IyBSdW5uaW5nIHRoZSB0b29sIGFnYWluc3Qgb3VyIEVDMiByZXNvdXJjZXMgb24gQVdTIGFuZCBzcGVjaWZ5aW5nIGFuIEFXUyBwcm9maWxlIHRvIHdvcmsgd2l0aA0KdGVycmFmb3JtaW5nIGVjMiAtLXByb2ZpbGUgX19fX19fX19fX19fXw0KDQojIFJ1bm5pbmcgdGhlIHRvb2wgYWdhaW5zdCBvdXIgUzMgcmVzb3VyY2VzIG9uIEFXUw0KdGVycmFmb3JtaW5nIHMzIC0tcHJvZmlsZSBfX19fX19fX19fX19fDQo=

4. Running Terraforming as a Docker container

Solution:
ZG9ja2VyIHJ1biBcCiAgICAtLXJtIFwKICAgIC0tbmFtZSB0ZXJyYWZvcm1pbmcgXAogICAgLWUgQVdTX0FDQ0VTU19LRVlfSUQ9WFhYWFhYWFhYWFhYWFhYWFhYWFggXAogICAgLWUgQVdTX1NFQ1JFVF9BQ0NFU1NfS0VZPXh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHh4eHggXAogICAgLWUgQVdTX1JFR0lPTj14eC15eXl5LTAgXAogICAgcXVheS5pby9kdGFuNC90ZXJyYWZvcm1pbmc6bGF0ZXN0IFwKICAgIHRlcnJhZm9ybWluZyBzMwo=


##### Submission:
- Provide the output of running the Terraforming tool against your AWS EC2 and S3 resources

Bonus challenge:
- Take one of the EC2 resources you got from the export, create a Terraform plan from it and implement a variables file for it,
Include a variable for "Instances count" - this variable will be used to determine how many instances to create like this EC2 instance