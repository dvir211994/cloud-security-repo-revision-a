### Demo - Checking Terraform configuration files using static analysis

Go over the documentation over at https://github.com/accurics/terrascan

##### Objectives:
- Use security best practice when working with Terraform
- Understand the implications of using a vulnerable Terraform configuration files
- Working with static analysis tools and analyzing the output


##### Steps:

1. Take one of the Terraform plan that we created

2. `cd` to the Terraform project plan folder

3. Run the following command `terrascan scan -t aws`

4. Go over the security recommendations of the tool to make sure we have no vulnerabilities

5. Fix any issues that appear in the report

6. Run the tool again and verify that the previously mentioned issues are not showing in the report anymore