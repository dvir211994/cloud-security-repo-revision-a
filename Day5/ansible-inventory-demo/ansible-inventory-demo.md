### Ansible inventory demo

##### Objectives:
- Creating 3 AWS EC2 instances
    * for each of the instances, configured "Name" and "Purpose" (Production, QA) tags
- Creating and configuring an Ansible inventory file including the 3 created instances
- Run an ad-hoc command to invoke the "ping"/"setup" modules on specific/all groups and review the output
- Creating the inventory using a dynamic inventory, in an automatic fashion, by

##### Steps:

1. First, create your hosts as a simple ini file containing hardcoded servers addresses of the EC2 instances that you have created in your AWS account

```tsx
[ubuntu_servers]
54.209.115.25
153.154.55.13
125.16.48.11

[dbservers]
one.example.com
two.example.com
three.example.com
```

2. Create a simple playbook file

```yaml
---
- name: Creating a log file on tmp
  hosts: ubuntu_servers
  remote_user: ubuntu
  become: true

  tasks:
  - name: Write to a log file on the tmp folder
    shell: echo "[INFO] $(date +%T) app initialized" > /tmp/log.log
```

3. Run ad-hoc command to invoke the ping/setup modules on specific groups

``` shell
# Running the ping module, if successful we should get a "pong" back
ansible ubuntu_servers -i hosts -m ping

# Running the setup module which gathers facts on the host
ansible ubuntu_servers -i hosts -m setup -a 'filter=*ipv4*'
```

4. Run the ansible-playbook command against a specific group in our hosts file

``` shell
ansible-playbook playbook.yml -i hosts
```

5. Now, let's create a dynamic inventory file, create a file called `inventory.aws_ec2.yml`

```yaml
plugin: aws_ec2
regions:
- us-east-1
keyed_groups:
- key: tags.Name
  prefix: name_
  separator: ""
- key: tags.Purpose
  prefix: purpose_
  separator: ""
  compose:
  ansible_host: public_ip_address
```

6. Now, in order to run the playbook against dynamically fetched instances on AWS based on tags, we will run the following command

Before we do that, we need to update our playbook to align with the hosts name convention that we are going to get from the dynamic hosts file

Let's run the following command to see what is the naming convention we get when running the inventory to get our hosts from AWS based on their tags

``` shell
ansible-inventory -i inventory.aws_ec2.yml --graph
```

Now, that we know how we get the host names convention from the dynamic inventory, let's change our playbook `hosts` section to align with the hosts we want to run our code on

In your playbook change the `hosts` section to be `purpose_QA`for example if you want to your playbook to run on the instances that have a tag name "Purpose" with value "QA"

```yaml
hosts: purpose_QA
```

Now, that we have aligned our playbook file and saw the dynamic inventory is working, let's run our playbook on the 

``` shell
ansible-playbook playbook.yml -i inventory.aws_ec2.yml
```

7. SSH into one of the instances that have the "Purpose" tag set to "QA" and verify that indeed they have the log.log file under the /tmp folder

##### Bonus challenge:
- Create an additional task in the playbook to create a new docker container of `postgresql` using the docker module
- Create a task to run a shell command which will output the default DBs that exist in the postgresql container