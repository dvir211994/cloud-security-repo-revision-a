### Demo - attaching a new disk to a running instance

##### Objectives:
- Create an EBS volume using the AWS Console
- Attach the volume to a running EC2 instance
- Configure the EC2 instance to mount the new volume attached to it

##### Steps:

1. Log in to the EC2 console –> Volumes and create a new volume of your preferred size and type

2. Right-click on the created volume and select “attach volume” option and select your EC2 instance

3. SSH to the instance and list the available disks using the following command `lsblk`

4. Format the volume to the `ext4` filesystem using the following command.

``` shell
sudo mkfs -t ext4 /dev/xvdf
```

5. Create a directory of your choice to mount our new ext4 volume. I am using the name “myNewVolume”

``` shell
sudo mkdir /myNewVolume
```

6. Mount the volume to “myNewVolume” directory using the following command.

``` shell
sudo mount /dev/xvdf /myNewVolume/
```

7. `cd` into newvolume directory and check the disk space for confirming the volume mount.

``` shell
cd /newvolume
df -h .
```