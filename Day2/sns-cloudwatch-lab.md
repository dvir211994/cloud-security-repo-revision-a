### Lab - CloudWatch and SNS integration

##### Objectives:

- Understand the integration between CloudWatch and SNS
- Create a CloudWatch alarm for an EC2 instace
- Trigger the alarm manually and verify the notification is sent

##### Steps:

1. Create a simple t2.micro EC2 instance

2. Create a CloudWatch alarm

3. Configure the alarm to watch the metric of CPU utilization for the EC2 instance you just created
    * Make sure to create the alarm in a manner that will pass the threshold (e.g setting a low threshold), or generate a load on the instance (less recommended)

4. When threshold is reached, the alarm will send a notification email using SNS (make sure you approve the subscription before testing)
* You can use temp mail services like mailinator

5. Verify that you are getting an email notification for the specific alarm


##### Submission:
- Submit the ARN of your the CloudWatch alarm the you created

##### Bonus challenge:

* Create an alarm for monitoring the disk space usage of the machine
    * Configure the alarm threshold as you wish, to any data that will trigger the alarm
    * Note that AWS does not provide a disk usage metric out-of-the-box, search the web to understand how this can be done
* Create an SNS topic and a mail subscription
* Verify that when the alarm state changes to "In alarm" an email is sent to the configured address
* Want to speed up the alarm triggering? take a look at `aws cloudwatch set-alarm-state`