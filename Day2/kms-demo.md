### Demo - Using KMS keys in AWS

##### Objectives:

- Creating KMS keys
- Encrypting and decrypting data using KMS

##### Steps:

* Notice that the keys are regional, so if we encrypt something with a key from one region we can’t decrypt the same object if we move it another region and run the decryption action from there

1. Let’s create a key, we can do that using the AWS console or using the CLI
    1. Go to AWS console -> KMS console
    2. Click on create a key and create a symmetrical key
    3. Grab the Key ID from the console
    4. Use AWS cli to run the following commands

* to also create the key from the CLI type
``` shell
aws kms create-key —description “Demo key”
```

2. Creating an alias for the key
``` shell
# you can think of alias keys like a shortcut name for you keys, it can point to different keys, this can come handy when using scripts and doing key rotations which will not force you to change hard coded Key ID values
```

``` shell
aws kms create-alias --target-key-id %KeyID% —alias-name “alias/demo”
```

now if we did that right, we should be able to see the key alias in the list and also in the AWS console
``` shell
aws kms list-keys
```

3. Go over to AWS console and review the new key and examine the policy configuration
Go over the key rotation tab, we can select to automatically rotate this CMK every year, for AWS managed keys, those keys are rotated by AWS every 3 years

4. Now, let’s open up the command line and start using this key
``` shell
echo “this is a secret message” > topsecret.txt
cat topsecret.txt
```

``` shell
aws kms encrypt --key-id “alias/demo” —plaintext file://topsecret.txt —output text —query CiphertextBlob
# we can also sepcify the Key ID instead of the alias
```

So this will encrypt the file with the KMS key that we have just created and going to output the ciphertextblob in an encrypted base64 form

5. Now, we want to take the this encrypted blob, do a base64 decode and save and save that raw encrypted binary to a file
``` shell
aws kms encrypt —key-id “alias/demo” —plaintext file://topsecret.txt —output text —query CiphertextBlob | base64 —decode > topsecret.txt.encrypted
```

If we cat the file we can see it is and encrypted binary in gibberish that does not give us too much information

6. Now, let’s decrypt the file using our KMS key
``` shell
aws kms decrypt --ciphertext-blob fileb://topsecret.txt.encrypted —output text —query Plaintext
# notice that we specify “fileb” since it is a binary
# now that will give us the decoded data, again in base64
```

7. So in order to get the plain text form now, we will base64 decode the output we get from the last command
``` shell
aws kms decrypt --ciphertext-blob fileb://topsecret.txt.encrypted —output text —query Plaintext | base64 —decode
# notice that we did not specify the CMK, that is because every time a message is being encrypted, there is a reference to the CMK that encrypted the data
```