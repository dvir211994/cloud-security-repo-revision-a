### S3 Demo - hosting a static website on S3, using the AWS Console

##### Objectives:

- Create a simple website, saving the hustle of taking care of servers or infrastructure
- Take off the load of responsibility for securing and hardening your web servers
- Create a sample website using `index.html` and host it on an S3 bucket

##### Steps:

1. Create a bucket and make it publicly accessible

2. Upload a sample index.html file to the root level of the bucket

3. Enable static website hosting on the bucket configuration and specify the index.html file

##### Resources:

`index.html` file:

``` shell
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>My Website</title>
</head>
<body>
  <h1>Welcome to my website</h1>
</body>
</html>
```

Bucket policy for public access:

``` shell
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "PublicReadGetObject",
            "Effect": "Allow",
            "Principal": "*",
            "Action": [
                "s3:GetObject"
            ],
            "Resource": [
                "arn:aws:s3:::%BUCKET_NAME_HERE/*"
            ]
        }
    ]
}
```