### Demo - AWS STS in action using AWS CLI

##### Objectives:
- Understand the basics of IAM and STS
- Assume a Role and verify permissions elevation

##### Steps:

1. Create a IAM user with low privileges - Go to AWS Console → Security, Identity, & Compliance → IAM → Users → Add user
  * for the access type, you can only give this user Programmatic access if you want to
  * In the next step don’t add this user to any group or attach any existing policyKeep everything default, Review and Create user
  * make sure to save the access key id and secret access key that you are presented with after creating the user, they will not be available again after they were created

2. Create a IAM role and attach it to a policy, for example "AmazonS3ReadOnlyAccess"

3. Update Trust Relationships, go to the IAM Role we created and click on the "Trust relationships" tab, paste in this policy, and change the ARN of the user to the user you created earlier

```
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::XXXXXX:user/myteststsuser"
      },
      "Action": "sts:AssumeRole",
      "Condition": {}
    }
  ]
}
```

4. Adding a policy for the user we created
go to the user we created and create a policy for him
service = sts
action = AssumeRole
resource = the ARN of the Role you created
* this allows our user assume the Role we created

5. Testing

A) Run the following command to add a profile to our AWS CLI
Insert the keys you saved from step number 1 where you created the user with programmatic access

``` shell
aws configure --profile sts-test-user
export AWS_PROFILE=sts-test-user
```

B) Before starting to test, let's verify that indeed we get `Access Denied` when trying to access any S3 bucket from a bucket we created previously

``` shell
aws s3 ls s3://%Bucket-Name%/%File-inside-bucket%
```

C) Generating a token

``` shell
aws sts assume-role --role-arn %Role-ARN% --role-session-name "STS-test-session"
```

D) Exporting (setting ENV variables) the keys we got from the output

```
export AWS_ACCESS_KEY_ID="XXXXXXX"
export AWS_SECRET_ACCESS_KEY="XXXXXXX"
export AWS_SECURITY_TOKEN="XXXXXXX"
```

E) Try to access the same S3 bucket again and see that now you have the sufficient permissions for that operation

``` shell
aws s3 ls s3://%Bucket-Name%/%File-inside-bucket%
```

F) When finished, run the following command to remove the ENV variables so they don't take precedence from the other profiles you have configured

``` shell
unset AWS_ACCESS_KEY_ID
unset AWS_SECRET_ACCESS_KEY
unset AWS_SECURITY_TOKEN
unset AWS_PROFILE
```
