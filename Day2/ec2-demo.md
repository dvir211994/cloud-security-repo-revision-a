### Demo - Creating an EC2 instance using AWS Console

##### Objectives:
- Create an EC2 instance on AWS

##### Steps:

1. Log in to the AWS console -> go to EC2 service

2. Create a key pair

3. Create an EC2 instance - Ubuntu 18 - t2.micro

4. Connect to the instance using SSH, while passing the key we created earlier

5. Fetch meta data of the instance, examine the output and see how it can be used for automations



``` shell
TOKEN=`curl -X PUT "http://169.254.169.254/latest/api/token" -H "X-aws-ec2-metadata-token-ttl-seconds: 21600"`
curl -H "X-aws-ec2-metadata-token: $TOKEN" -v http://169.254.169.254/latest/meta-data/
```

---


* Sneak peek
Instead of creating the EC2 instance manually, let's see how we can easily create it using the AWS CLI


``` shell
# Configure AWS CLI client
aws configure
# Create a key pair, can also be done through the AWS console, using the key pair ID we created earlier
aws ec2 create-key-pair --key-name TestKeyPair
# Create EC2 instance
aws ec2 run-instances --image-id ami-xxxxxxxx --count 1 --instance-type t2.micro --key-name TestKeyPair --security-group-ids sg-XXXXXX --subnet-id subnet-6e7f829e

#can be shortened to this, to launch with default subnet and SG
#notice that we need to specify a valid AMI the region we deploy to
aws ec2 run-instances \
    --image-id ami-0abcdef1234567890 \
    --instance-type t2.micro \
    --key-name TestKeyPair
```
