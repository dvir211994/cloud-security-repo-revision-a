### Demo - EC2 instance lab

Objectives:
- Creating a IAM user and generating access keys
- Creating an AWS EC2 instance using the AWS CLI

##### Steps:

1. Create a IAM user and generate access tokens for it, save generated tokens for later use

2. Create an EC2 instance using the AWS CLI (you can use default VPC and security group)

3. SSH into the EC2 instance you created using the key-pair specified in the creation stage

4. Harden your security group rules to only allow SSH from your external IP address
* Can be done via AWS Console or the CLI

##### Submission:
- Submit the public IP of the instance you created