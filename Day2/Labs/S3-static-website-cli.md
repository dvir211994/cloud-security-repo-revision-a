### Lab - S3 static website hosting

##### Objectives:
- Create an S3 bucket and configure it to serve a website

##### Steps:

1. Create an S3 bucket using the AWS CLI (https://docs.aws.amazon.com/cli/latest/reference/s3api/create-bucket.html)

2. Upload an index.html file to that bucket under the root folder location

3. Set the bucket to be website hosting bucket using AWS CLI (https://docs.aws.amazon.com/cli/latest/reference/s3/website.html)

4. List the bucket contents (using the CLI) and verify that the file was uploaded under the root folder

5. Go to the AWS console and see the S3 endpoint URL, browse to that URL and verify that you are seeing the website

6. Download the file from the bucket and edit it, add an HTML title, tag and save the file
* https://www.w3schools.com/tags/tag_title.asp

7. Re-upload the file, replacing the current one with the new file you just edited

8. Visit the website URL again and verify that the code changed in the website (your indication will be the tab title)

##### Submission:
- Submit the URL of your S3 static website in Moodle
- Once the submission is reviewed and approved, please delete the bucket