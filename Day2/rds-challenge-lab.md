### Challenge lab: Create an automatic WordPress instance working with RDS

Pre-requisites:
- Understand what is RDS and a connection string
- Basic installation and configuration of a WordPress website
- Using SSH to connect to a remote host
- Using the "User-data" in EC2 to create a start-up script at instance launch
- Understanding of the basics of VPC and Security groups
- Familiarity with running Bash commands in Centos/Ubutnu (or whichever distribution you feel convinient with)

1. Create a MySQL RDS instance using AWS Console
    * This can also be done through API or AWS CLI, but note that it poses a security rist, since password is sent over the web in plain text
    * Search the web to understand different ways of encrypting your input to send the RDS creation parameters in an encrypted manner

2. Create an EC2 instance in the same VPC as the RDS

3. Create and configure a WordPress using the "User-data" in the EC2 creation part
    * Make sure to configure WordPress to work with the MySQL RDS instance you created earlier

4. Verify that when the server boots you are able to see the default WordPress page


Hints:

IyEvYmluL2Jhc2gKeXVtIHVwZGF0ZQp5dW0gaW5zdGFsbCBodHRwZCAteQpzZXJ2aWNlIGh0dHBkIHN0YXJ0Cnl1bSBpbnN0YWxsIHdnZXQgLXkKIyBGaW5kIHRoZSBwYWNrZ2UgdG8gaW5zdGFsbCBNeVNRTAp3Z2V0IGh0dHBzOi8vWFhYWFhYWFhYCiMgaW5zdGFsbCB0aGUgcnBtIHBhY2thZ2UKc3VkbyBycG0gLWl2aApzdWRvIHl1bSBpbnN0YWxsIG15c3FsLXNlcnZlciAteQp5dW0gaW5zdGFsbCBwaHAgcGhwLW15c3FsIC15CnNlcnZpY2UgbXlzcWxkIHN0YXJ0CgpteXNxbCAtdXJvb3QgPDxNWVNRTF9TQ1JJUFQKIyBXcml0ZSByZWxldmFudCBTUUwgcXVlcmllcyBoZXJlIHRvIGNvbmZpZ3VyZSBkZWZhdWx0IFdvcmRQcmVzcyBjb25maWd1cmF0aW9ucwpNWVNRTF9TQ1JJUFQKCiMgSW5zdGFsbCB3b3JkcHJlc3MKY2QgL3Zhci93d3cvaHRtbAp3Z2V0IGh0dHA6Ly93b3JkcHJlc3Mub3JnL2xhdGVzdC50YXIuZ3oKdGFyIC14enZmIGxhdGVzdC50YXIuZ3oKbXYgd29yZHByZXNzIG15YmxvZwpjZCBteWJsb2cKbXYgd3AtY29uZmlnLXNhbXBsZS5waHAgd3AtY29uZmlnLnBocAojIFVzZSB0ZXh0IG1hbmlwbHVhdGlvbiB0b29scyB0byBjaGFuZ2UgY29uZmlndXJhdGlvbnMgbGlrZSBkYXRhYmFzZSwgdXNlciBuYW1lIGFuZCBwYXNzd29yZCBpbiB0aGUgY29uZmlnIGZpbGUgd3AtY29uZmlnLnBocAojIEV4YW1wbGUgYmVsb3csIHlvdSBjYW4gYWxzbyB1c2UgJ3NlZCcgb3IgJ2F3aycgaWYgeW91IHdhbnQKcGVybCAtcGkgLWUgInMvZGF0YWJhc2VfbmFtZV9oZXJlL215YmxvZy9nIiB3cC1jb25maWcucGhwCgojIEFkZCBwb3N0IGluc3RhbGxhdGlvbiBzdGVwcyB0byBtYWtlIHN1cmUgdGhhdCB0aGUgcmVxdWlyZWQgc2VydmljZXMgYXJlIHVwLCBhbmQgdGhlaXIgY29uZmlndXJhdGlvbiBpcyBPSw==